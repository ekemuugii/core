import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerRelatedPerson } from "../../../models/CustomerRelatedPerson";

const Schema = Joi.object({
  whoTypeId      : Joi.string().max(100).required(),
  firstName    : Joi.string().max(100).required(),
  lastName     : Joi.string().max(100).required(),
  phone        : Joi.string().max(100).required()
});

  type IBody = {
    whoTypeId : string;
    firstName : string;
    lastName : string;
    phone : string;
    
  }
export default Method.put(`/customer_related_person/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id: string = req.params.id;

  const {
    whoTypeId,
    firstName,
    lastName,
    phone
   
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        
      const customerRelatedPerson = await CustomerRelatedPerson.findOne({ where: { id }, transaction });
      if (!customerRelatedPerson) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч бүртгэлгүй байна.");
        
      customerRelatedPerson.set({
        whoTypeId,
        firstName,
        lastName,
        phone
      });
      const result = await customerRelatedPerson.save({ transaction });
        
      await commit();
  
      res.json({ success: true });
    } catch (err) {
      await rollback();
  
      throw err;
    }
  });
});