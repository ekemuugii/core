import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { ERRORS } from "../../../constants";
import { CustomerRelatedPerson } from "../../../models/CustomerRelatedPerson";

export default Method.del(`/customer_related_person/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;


  const customerRelatedPerson = await CustomerRelatedPerson.findOne({ where: { id } });
  if (!customerRelatedPerson) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Хэрэглэгч олдсонгүй.");

  const result = await CustomerRelatedPerson.destroy({ where: { id: customerRelatedPerson.id } });
 
  res.json({ success: true });
});