import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductLimit } from "../../../models/LoanProductLimit";

export default Method.get(`/LoanProductLimit/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanProductLimit.findOne({ where: { id } });

  res.json(loan);
});