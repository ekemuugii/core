import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductLimit } from "../../../models/LoanProductLimit";


const schema = Joi.object({
  minLimit: Joi.number().required(),
  maxLimit: Joi.number().required(),
  loanProductId: Joi.string().required(),

});

type IBody = {
  minLimit : number;
  maxLimit : number;
  loanProductId : string;
} 

export default Method.post("/LoanProductLimit/create", schema, async (req: Request, res: Response) => {
  const {
    minLimit,
    maxLimit,
    loanProductId
    
  }: IBody = req.body
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loanProductLimit = await LoanProductLimit.findOne({ where: { loanProductId :loanProductId}, transaction });
      if (loanProductLimit) throw new ValidationError(ERRORS.LOAN_PRODUCT_ALREADY_REGISTERED, "ЗЭЭЛИЙН БҮТЭЭГДЭХҮҮНИЙ ЛИМИТ БҮРТГЭГДСЭН БАЙНА.");

      const loanProductLimitNew = new LoanProductLimit({
        minLimit: minLimit,
        maxLimit: maxLimit,
        loanProductId :loanProductId
  
      });

      const result = await loanProductLimitNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});