import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductLimit } from "../../../models/LoanProductLimit";

export default Method.del(`/LoanProductLimit/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const loanProductLimit = await LoanProductLimit.findOne({ where: { id } });
  if (!loanProductLimit) throw new NotfoundError(ERRORS.LOAN_PRODUCT_NOTFOUND, "ЗЭЭЛИЙН БҮТЭЭГДХҮҮНИЙ ЛИМИТ ОЛДСОНГҮЙ.");
    
  await LoanProductLimit.destroy({ where: { id } });
    
  res.json({ success: true });
});