// import { kafkaWrapper, UserAuthChangedProducer } from "@goodtechsoft/cb-broker";
import { Method, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { v4 as uuidv4 } from "uuid";
import { ERRORS } from "../../../constants";
import { SESSION_SCOPE } from "../../../constants/SESSION_SCOPE";
import { Request, Response, signIn } from "../../../middlewares/sign";

const schema = Joi.object({
  password   : Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).required(),
  oldPassword: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).optional().allow(null)
});

type IBody = {
  password: string;
  oldPassword: string;
}

export default Method.post("/password/change", schema, async (req: Request, res: Response) => {
  const {
    password,
    oldPassword
  }: IBody = req.body;

  if (req.sessionScope !== SESSION_SCOPE.CHANGE_PASSWORD) {
    if (!oldPassword) throw new ValidationError(ERRORS.INVALID_ACTION, "Хуучин нууц үг оруулна уу!");
    if (!(await req.user?.validatePassword(oldPassword)))
      throw new ValidationError(ERRORS.INVALID_OLD_PASSWORD, "Хуучин нууц үг буруу байна!");

    await req.user?.createPassword(password);
    await req.user?.save();
      
    res.json({ success: true });
    return;
  }

  await req.user?.createPassword(password);
  await req.user?.save();

  const SESSION_ID = uuidv4();
  
  signIn(res, {
    user     : req.user!,
    sessionId: SESSION_ID
  }, SESSION_SCOPE.AUTHORIZED);
  
  // /**
  // new UserAuthChangedProducer(kafkaWrapper).send(req.user!.id, {
  //   id               : req.user!.id,
  //   sessionId        : SESSION_ID,
  //   currentBusinessId: undefined,
  //   partnerId        : req.user?.partnerId
  // });
  //  */
});