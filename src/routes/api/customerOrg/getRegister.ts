import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { ERRORS } from "../../../constants";
import { CustomerOrg } from "../../../models/CustomerOrg";

type IBody = {
    id : string;
}
export default Method.get(`/customerOrg/getRegister/:${("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id
  const customer = await CustomerOrg.findOne({ where: { registerNo:id } }); 
  if (!customer) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    res.json({
      result: customer
    });
});
    