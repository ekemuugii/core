import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerOrg } from "../../../models/CustomerOrg";
import { startTransaction } from "../../../core/sequelize";
import { ERRORS } from "../../../constants";
import { OrgType } from "../../../models/OrgType";
import { Industry } from "../../../models/Industry";
import { EmployeeUnit } from "../../../models/EmployeeUnit";

type IBody = {
    id : string;
}
export default Method.get(`/customerOrg/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id

  const customerOrg= await CustomerOrg.findOne({ include : [ {
    model : OrgType,
    as : "orgType"
  },
  {
    model : Industry,
    as    : "industry"
  },
  {
    model : EmployeeUnit,
    as    : "employeeUnit"
  }
] ,where: { id } });
  if (!customerOrg) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    res.json({
      result: customerOrg
    });
});
    