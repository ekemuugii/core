import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerOrg } from "../../../models/CustomerOrg";
    

export default Method.del(`/customerOrg/:${Method.uuid("id")}`,null, async (req: Request, res: Response) => {
  const id: string = req.params.id;   
  const customerOrg = await CustomerOrg.findOne({ where: { id } });

  if (!customerOrg) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    await CustomerOrg.destroy({ where: { id: customerOrg.id } });

  res.json({ success: true });
});

