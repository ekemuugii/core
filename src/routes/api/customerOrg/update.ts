import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import {  CustomerOrg } from "../../../models/CustomerOrg";
import Joi from "joi";
 

const Schema = Joi.object({
  name: Joi.string().max(100).required(),
  registerNo: Joi.string().max(100).required(),
  stateRegNo: Joi.string().max(100).required(),
  stateRegDate: Joi.date().required(),
  orgTypeId: Joi.string().required(),
  industryId: Joi.string().required(),
  email: Joi.string().max(100).required(),
  phone: Joi.string().max(100).required(),
  directorFirstName: Joi.string().max(100).required(),
  directorLastName: Joi.string().max(100).required(),
  directorRegisterNo: Joi.string().required(),
  directorPhone: Joi.number().required(),
  contactEmpFirstName: Joi.string().max(100).required(),
  contactEmpLastName: Joi.string().max(100).required(),
  contactEmpRegisterNo: Joi.string().max(100).required(),
  contactEmpPhone: Joi.number().required(),
  incomeAmountYear: Joi.number().required(),
  employeeCount: Joi.number().required(),
  employeeUnitId: Joi.string().max(100).required(),

});


type IBody = {
  name         : string;
  registerNo          : string;
  stateRegNo        : string;
  stateRegDate        : Date;
  orgTypeId             : string;
  industryId             : string;
  email         : string;
  phone     : number;
  directorFirstName    : string;
  directorLastName       : string;
  directorRegisterNo : string;
  directorPhone            : number;
  contactEmpFirstName   : string;
  contactEmpLastName            : string;
  contactEmpRegisterNo        : string;
  contactEmpPhone    : number;
  incomeAmountYear    : number;
  employeeCount    : number;
  employeeUnitId   : string;
}

export default Method.put(`/customerOrg/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const {
    name,
    registerNo,
    stateRegNo,
    stateRegDate,
    industryId,
    email,
    orgTypeId,
    phone,
    directorFirstName,
    directorLastName,
    directorRegisterNo,
    directorPhone,
    contactEmpFirstName,
    contactEmpLastName,
    contactEmpRegisterNo,
    contactEmpPhone,
    incomeAmountYear,
    employeeCount,
    employeeUnitId
  }:IBody = req.body;
  
  console.log(req.body);
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        
      const customerOrg = await CustomerOrg.findOne({ where: { id }, transaction });
      if (!customerOrg) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч бүртгэлгүй байна.");
        
      customerOrg.set({
        name,
        registerNo,
        stateRegNo,
        stateRegDate,
        industryId,
        email,
        orgTypeId,
        phone,
        directorFirstName,
        directorLastName,
        directorRegisterNo,
        directorPhone,
        contactEmpFirstName,
        contactEmpLastName,
        contactEmpRegisterNo,
        contactEmpPhone,
        incomeAmountYear,
        employeeCount,
        employeeUnitId
      });
      const result = await customerOrg.save({ transaction });
        
      await commit();
  
      res.json({ success: result });
    } catch (err) {
      await rollback();
  
      throw err;
    }
  });
});