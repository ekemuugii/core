import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerOrg } from "../../../models/CustomerOrg";

const schema = Joi.object({
  name: Joi.string().max(100).required(),
  registerNo: Joi.string().regex(/^[0-9]{7}$/).required(),
  stateRegNo: Joi.string().max(100).required(),
  stateRegDate: Joi.date().required(),
  orgTypeId: Joi.string().required(),
  industryId: Joi.string().required(),
  email: Joi.string().regex(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).required(),
  phone: Joi.string().max(100).required(),
  directorFirstName: Joi.string().max(100).required(),
  directorLastName: Joi.string().max(100).required(),
  directorRegisterNo: Joi.string().regex(/^[А-Яа-я|Үү|Өө|Ёё]{2}\d{8}$/).required(),
  directorPhone: Joi.number().required(),
  contactEmpFirstName: Joi.string().max(100).required(),
  contactEmpLastName: Joi.string().max(100).required(),
  contactEmpRegisterNo: Joi.string().regex(/^[А-Яа-я|Үү|Өө|Ёё]{2}\d{8}$/).required(),
  contactEmpPhone: Joi.number().required(),
  incomeAmountYear: Joi.number().required(),
  employeeCount: Joi.number().required(),
  employeeUnitId : Joi.string().max(100).required(),
});


type IBody = {
  name         : string;
  registerNo          : string;
  stateRegNo        : string;
  stateRegDate        : Date;
  orgTypeId             : string;
  industryId             : string;
  employeeUnitId          : string;
  email         : string;
  phone     : number;
  directorFirstName    : string;
  directorLastName       : string;
  directorRegisterNo : string;
  directorPhone            : number;
  contactEmpFirstName   : string;
  contactEmpLastName            : string;
  contactEmpRegisterNo        : string;
  contactEmpPhone    : number;
  incomeAmountYear    : number;
  employeeCount    : number;
}

export default Method.post("/customerOrg/create", schema, async (req: Request, res: Response) => {
  const {
    name,
    registerNo,
    stateRegNo,
    stateRegDate,
    industryId,
    email,
    orgTypeId,
    phone,
    directorFirstName,
    directorLastName,
    directorRegisterNo,
    directorPhone,
    contactEmpFirstName,
    contactEmpLastName,
    contactEmpRegisterNo,
    contactEmpPhone,
    incomeAmountYear,
    employeeCount,
    employeeUnitId
  }:IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const customerOrg = await CustomerOrg.findOne({ where :{registerNo:registerNo }, transaction }); 
      if (customerOrg) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Харилцагч регистр бүртгэгдсэн байна.");
      

      const customerOrgNew = new CustomerOrg({  
        name                 : name,
        registerNo           : registerNo,
        stateRegNo           : stateRegNo,
        stateRegDate         : stateRegDate,
        industryId           : industryId,
        email                : email,
        phone                : phone,
        orgTypeId            : orgTypeId,
        directorFirstName    : directorFirstName,
        directorLastName     : directorLastName,
        directorRegisterNo   : directorRegisterNo,
        directorPhone        : directorPhone,
        contactEmpFirstName  : contactEmpFirstName,
        contactEmpLastName   : contactEmpLastName,
        contactEmpRegisterNo : contactEmpRegisterNo,
        contactEmpPhone      : contactEmpPhone,
        incomeAmountYear     : incomeAmountYear,
        employeeCount        : employeeCount,
        employeeUnitId       : employeeUnitId
        
      });
 
      const result =  await customerOrgNew.save({ transaction });
  
      await commit();

      res.json({ success: result });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});