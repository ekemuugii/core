import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { number } from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanDetail } from "../../../models/LoanDetail";
import { LoanPayGraph } from "../../../models/LoanPayGraph";
import { Loan } from "../../../models/Loan";
import moment from "moment";
import { getDaysInMonth } from "../../../middlewares/date";


const schema = Joi.object({
    loanId         : Joi.string().max(250).required(),
    currencyId       : Joi.string().required(),
    currencyRate   : Joi.number().required(),
    rate           : Joi.number().required(),
    amount         : Joi.number().required(),
    paymentDay1    : Joi.number().max(31).required(),
    paymentDay2    : Joi.number().max(31).optional().allow(null, ""),
    startDate      : Joi.date().required(),
    endDate        : Joi.date().required(),
    loanPayTypeId: Joi.string().required(),
    loanPayFreqId: Joi.string().required(),
    loanFreePayCount: Joi.number().required(),
    holidayCalcId :Joi.string().required(),


});

type IBody = {
   loanId           : string;
   currencyId       : string;
   currencyRate     : number;
   rate             : number;
   paymentDay1      : number;
   paymentDay2      : number;
   startDate        : Date;
   endDate          : Date;
   amount           : number;
   totalResidual    : number;
   loanPayTypeId    : string;
   loanPayFreqId    : string;
   loanFreePayCount : number;

   holidayCalcId  :string;
 
   
} 

export default Method.post("/Loan_detail/create1", schema, async (req: Request, res: Response) => {
  const {
    loanId,
    currencyId,
    currencyRate,
    rate,
    paymentDay1,
    paymentDay2,
    startDate,
    endDate,
    amount,
    totalResidual,
    loanPayTypeId,
    loanPayFreqId,
    loanFreePayCount,
    holidayCalcId


  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {

      const loanDetailNew = new LoanDetail({
        loanId        : loanId,
        currencyId      : currencyId,
        currencyRate  : currencyRate,
        rate          : rate,
        amount        : amount,
        paymentDay1   : paymentDay1,
        paymentDay2   : paymentDay2,
        startDate     : startDate,
        endDate       : endDate,
        totalResidual :totalResidual,
        loanPayTypeId :loanPayTypeId,
        loanPayFreqId :loanPayFreqId,
        loanFreePayCount:loanFreePayCount,
        holidayCalcId :holidayCalcId
      });


      await loanDetailNew.save({ transaction });


      if(!paymentDay2) {  
        

        if(parseInt(moment(new Date(startDate)).format("DD")) < paymentDay1){                 
          const diffDay =paymentDay1 - parseInt(moment(new Date(startDate)).format("DD")) ;
          const paytimes1 = parseInt(moment(new Date(startDate)).format("MM"));
          const paytimes2 = parseInt(moment(new Date(endDate)).format("MM"));
          const paytime = paytimes2-paytimes1                                                       //-----------------------|
          const diffRateDay = (moment.duration(moment((moment(new Date(startDate)).add(1,"month").format("YYYY-MM-DD"))).diff(startDate))).asDays() - diffDay;                                                                                       //-----------------------|
          let startMoment = moment(startDate);                                                       //---------ЭХЛЭЛ---------|
          let endMoment = moment(endDate);                                                          //-----------------------|
          let duration = moment.duration(endMoment.diff(startMoment));                              //_______________________|
          let days = duration.asDays();
          const rateDay = ((amount / 100 * rate) * paytime) / (days - diffRateDay);
          const now = moment(new Date()).format("YYYY-MM-DD");
          let PaymentDay1 = new Date(moment(new Date(startDate)).add(diffDay,"day").format("YYYY-MM-DD"));
          let TotalPayment = amount+(amount / 100 * (rate)*paytime) ;
          let TotalAPayment = amount+(amount / 100 * (rate)*paytime) ;
          let Payment = (amount / paytime)+(diffDay * rateDay);

          for(let n = 1; n <= paytime; n++){
            console.log(paymentDay1);
            const firstDay= new Date(moment(new Date(startDate)).add(n,"month").add(diffRateDay,"day").format("YYYY-MM-DD"));
            const deffDay = getDaysInMonth(parseInt(moment(new Date(firstDay)).format("YYYY")),parseInt(moment(new Date(firstDay)).format("MM"))+n-1)-parseInt(moment(new Date(firstDay)).format("DD"))+paymentDay1;
            const loanPayGraphNew = new LoanPayGraph ({
            loanId                 : loanId,
            mainLoanPayAmount      : TotalAPayment/paytime,
            totalPayAmount         : TotalPayment,
            loanResidual           : TotalPayment - Payment,
            rateCalcDay            : deffDay,
            payDate                : PaymentDay1,
            rateAmount             : (TotalPayment/100*rate)/deffDay*deffDay
           
          });
          console.log(loanPayGraphNew);
          await loanPayGraphNew.save({ transaction });
          TotalPayment -= Payment;
          PaymentDay1 = new Date(moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD"));
          startMoment = moment(PaymentDay1);
          endMoment = moment(new Date(moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD")));
          duration = moment.duration(endMoment.diff(startMoment));
          days = duration.asDays();
          Payment = amount / paytime + days * rateDay;
          }        
          // const loanPayGraphNew = new LoanPayGraph ({
          //   loanId       : loanId,
          //   payment      : TotalPayment,
          //   totalPayment : TotalPayment,
          //   loanResidual : 0,                                           //__________________________________________________________
          //   loanRate     : rate,                                        //----------------------||||||||||||||||--------------------|
          //   payDate      : endDate                                      //----------------------||||||||||||||||--------------------|
                                                                        //-----------------------Энэ хүртэл !!!---------------------|
          // })                                                            //----------------------||||||||||||||||--------------------|
          // await loanPayGraphNew.save({ transaction });                  //----------------------||||||||||||||||--------------------|
        }  

        // -----------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------
        else if(parseInt(moment(new Date(startDate)).format("DD")) > paymentDay1){
          let diffDay = parseInt(moment(new Date(startDate)).format("DD")) - paymentDay1
          let startMoment = moment(startDate);
          let endMoment = moment(endDate);
          let duration = moment.duration(endMoment.diff(startMoment));
          let days = duration.asDays();

          const firstPayDate =new Date(moment(new Date(startDate)).add(1,"month").date(paymentDay1).format("YYYY-MM-DD")) ;
          startMoment = moment(startDate);
          endMoment = moment(firstPayDate);
          duration = moment.duration(endMoment.diff(startMoment));
          let firstPayDays = duration.asDays();

          const paytimes1 = parseInt(moment(new Date(startDate)).format("MM"));                     //----------------|
          const paytimes2 = parseInt(moment(new Date(endDate)).format("MM"));                       //-----эхлэл------|
          const paytime = paytimes2-paytimes1                                                       //----------------|                                                                                                                                                                                                       //

          const rateDay = ((amount / 100 * rate) * paytime) / (days-diffDay);

          let PaymentDay1 = firstPayDate;
          let TotalPayment = amount+(amount / 100 * (rate)*paytime) ;
          let Payment = (amount / paytime)+(firstPayDays * rateDay);
          
          for(let n = 1; n <= paytime; n++){
            
            const loanPayGraphNew = new LoanPayGraph ({
            loanId       : loanId,
            // // payment      : Payment,
            // // totalPayment : TotalPayment,
            // loanResidual : TotalPayment - Payment,
            // loanRate     : rate,
            // payDate      : PaymentDay1
            
          });
          await loanPayGraphNew.save({ transaction });
          TotalPayment -= Payment;
          PaymentDay1 = new Date(moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD"));
          startMoment = moment(PaymentDay1);
          endMoment = moment(new Date(moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD")));
          duration = moment.duration(endMoment.diff(startMoment));                                    //---------------------|
          days = duration.asDays();                                                                  //------Төгсгөл---------|
          Payment = amount / paytime + days * rateDay;                                              //-----------------------|
          }        
          // const loanPayGraphNew = new LoanPayGraph ({
          //   loanId       : loanId,
          //   payment      : TotalPayment,
          //   totalPayment : TotalPayment,
          //   loanResidual : 0,
          //   loanRate     : rate,
          //   payDate      : endDate
                        
          // })
          // await loanPayGraphNew.save({ transaction });
        }  
        }
     

        if(paymentDay2) { 
          if(parseInt(moment(new Date(startDate)).format("MM")) < paymentDay1){ 
            let diffDay = paymentDay1 - parseInt(moment(new Date(startDate)).format("DD"))
            let startMoment = moment(startDate);
            let endMoment = moment(endDate).date(paymentDay2);
            let duration = moment.duration(endMoment.diff(startMoment));
            let days = duration.asDays();
            // const diffRateDay = (moment.duration(moment((moment(new Date(startDate)).add(1,"month").format("YYYY-MM-DD"))).diff(startDate))).asDays() - paymentDay2; 
            let diffpayday = paymentDay2 - paymentDay1;
            const paytimes1 = parseInt(moment(new Date(startDate)).format("MM"));
            const paytimes2 = parseInt(moment(new Date(endDate)).format("MM"));
            let PaymentDay1 = new Date(moment(new Date(startDate)).date(paymentDay1).format("YYYY-MM-DD"));
            const paytime = (paytimes2-paytimes1) * 2
            const rateDay = ((amount / 100 * rate) * (paytime)) / days; 
            let TotalPayment = amount+(amount / 100 * (rate*paytime));
            let Payment = (amount / paytime)+(diffDay * rateDay);
            
              for(let n = 1; n <= paytime; n++){
                
                let loanPayGraphNew = new LoanPayGraph ({
                loanId              : loanId,
                mainLoanPayAmount   : Payment,
                totalPayAmount      : (amount + days * rateDay) / paytime,
                loanResidual        : TotalPayment - Payment,
                rateAmount          : diffDay * days,
                payDate             : PaymentDay1
                });
                await loanPayGraphNew.save({ transaction });

                TotalPayment -= Payment;
                PaymentDay1 = new Date(moment(new Date(PaymentDay1)).date(paymentDay2).format("YYYY-MM-DD"));
                Payment = amount / paytime + diffpayday * rateDay;

                loanPayGraphNew = new LoanPayGraph ({
                  loanId       : loanId,
                  // payment      : Payment,
                  // totalPayment : TotalPayment,
                  // loanResidual : TotalPayment - Payment,
                  // loanRate     : rate,
                  // payDate      : PaymentDay1
                  
                 
                  });
                  await loanPayGraphNew.save({ transaction });
                startMoment = moment(PaymentDay1);
                endMoment = moment(new Date(moment(new Date(PaymentDay1)).date(paymentDay1).add(1,"month").format("YYYY-MM-DD")));
                duration = moment.duration(endMoment.diff(startMoment));
                days = duration.asDays();
                Payment = amount / paytime + days * rateDay;
                TotalPayment -= Payment;
                PaymentDay1 = new Date(moment(new Date(PaymentDay1)).date(paymentDay1).add(1,"month").format("YYYY-MM-DD"));
                
              } 
            }
                
          }

      await commit();
 
      res.json({ success: true });
    } catch (err) {
      await rollback();
 
      throw err;
    }
  });
});
// const start = moment(new Date()).add(1, "months").toISOString();

// const future = moment(new Date(start)).add(4, "months");//.format("YYYY-MM-DD");
// console.log(" >>> future >>>", future);

// const now = moment(new Date());//.format("YYYY-MM-DD");
// console.log(" >>> now >>>", now);

// const diff = moment(future).diff(now);
// console.log(" >>> diff >>>", diff);

// const duration = moment.duration(diff).asDays();
// console.log(" >>> duration >>>", duration);
        