import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanDetail } from "../../../models/LoanDetail";

export default Method.get(`/Loan_detail/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanDetail.findOne({ where: { loanId :id } });

  res.json(loan);
});