import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
import { LoanProduct } from "../../../models/LoanProduct";
import { Loan } from "../../../models/Loan";
import { addAttribute } from "sequelize-typescript";


const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/dashboard/data", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query;
    
    filters[Op.or] = [
    //   Sequelize.where(Sequelize.col("product_type_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("loan_type_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("loan_category_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("amount"), "LIKE", `%${query}%`),
      // Sequelize.where(Sequelize.col("customer_id"), "LIKE", `%${query}%`),
      // Sequelize.where(Sequelize.col("id"), "LIKE", `%${query}%`),


    ];
  }

  const { rows, count } = await Loan.findAndCountAll({
    where : filters,
    include: [{
      model: Customer,
      as   : "customer"
    },{
      model: LoanProduct,
      as   : "product",
      attributes : ["name", "id"]
    }]
,    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});


    