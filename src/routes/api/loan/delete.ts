import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Loan } from "../../../models/Loan";
    

export default Method.del(`/Loan/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await Loan.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await Loan.destroy({ where: { id} });
    
    res.json({ success: true });
});
