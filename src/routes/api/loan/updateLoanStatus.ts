import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { Loan } from "../../../models/Loan";
import { startTransaction } from "../../../core/sequelize";
      
      const schema = Joi.object({
        loanStatusId: Joi.string()
      });
      
        type IBody = {
          productId   : string;
            loanTypeId      : string ;
            loanCategoryId  : string ;
            amount          : number ;
            loanStatusId          : string ;
      };
      
      export default Method.put(`/Loan/updateStatus/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
            loanStatusId
        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const  loan = await Loan.findOne({ where: { id}, transaction });
          if (!loan) throw new NotfoundError( " Зээл олдсонгүй.");
    
          loan.set({
            loanStatusId
          });
      
          await loan.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
    
          throw err;
        }
        });
      });
      