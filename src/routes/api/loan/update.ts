import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { Loan } from "../../../models/Loan";
import { startTransaction } from "../../../core/sequelize";
      
      const schema = Joi.object({
        productId    : Joi.string(),
        loanTypeId       : Joi.string(),
        loanCategoryId   : Joi.string(),
        amount           : Joi.number(),
        loanStatusId: Joi.string()
      });
      
        type IBody = {
          productId   : string;
            loanTypeId      : string ;
            loanCategoryId  : string ;
            amount          : number ;
            loanStatusId          : string ;
      };
      
      export default Method.put(`/Loan/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
          productId,
            loanTypeId,
            loanCategoryId,
            amount,
            loanStatusId,
       
        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const  loan = await Loan.findOne({ where: { id}, transaction });
          if (!loan) throw new NotfoundError( " Зээл олдсонгүй.");
    
          loan.set({
            productId,
            loanTypeId,
            loanCategoryId,
            amount,
            loanStatusId
          });
      
          await loan.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
    
          throw err;
        }
        });
      });
      