import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { boolean } from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Loan } from "../../../models/Loan";

const schema = Joi.object({
  loanProductId        : Joi.string(),
  productId        : Joi.string(),
  loanTypeId       : Joi.string().required(),
  loanCategoryId   : Joi.string().required(),
  amount           : Joi.number().required(),
  customerId       : Joi.string().max(100).required(),
  loanDate         : Joi.date().required(),
  loanRate         : Joi.number().required(),
  currencyId       : Joi.string().required(),
  loanTimeId       : Joi.string().required(),
  isToZms          : Joi.boolean(),
  isJudge          : Joi.boolean(),
  dayCalcId        : Joi.string(),
  createdBy        : Joi.string()
});

type IBody = {
  productId: string;
  loanProductId      : string;
  loanTypeId         : string;
  loanCategoryId     : string;
  amount             : number;
  customerId         : string;   
  loanDate           : Date;
  loanRate           : number;
  currencyId         : string;
  loanTimeId         : string;
  dayCalcId          : string;
  isToZms            : boolean;
  isJudge            : boolean;
  createdBy             :  string;

} 

export default Method.post("/Loan/create", schema, async (req: Request, res: Response) => {
  const {
    loanProductId,
    loanTypeId,
    loanCategoryId,
    amount,
    customerId,
    loanDate,
    loanRate,
    currencyId,
    loanTimeId ,
    dayCalcId,
    isToZms,
    isJudge,
    createdBy
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loan = await Loan.findOne({ where: { customerId : customerId }, transaction });
        // if (loan) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Хэрэглэгч бүртгэгдсэн байна.");

      const loanNew = new Loan({
        productId       : "eeab94b7-a98f-4592-a2a0-f9f33542cb51",
        loanProductId   : loanProductId,
        loanTypeId      : loanTypeId,
        loanCategoryId  : loanCategoryId,
        amount          : amount,
        customerId      : customerId,
        loanDate        : loanDate,
        loanRate        : loanRate,
        currencyId      : currencyId,
        loanTimeId      : loanTimeId,
        dayCalcId       : dayCalcId,
        isToZms         : isToZms,
        isJudge         : isJudge,
        createdBy       : createdBy,
        loanStatusId    : "1"
      });

      const result =  await loanNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});