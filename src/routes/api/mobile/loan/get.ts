import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../../middlewares/sign";
import { Loan } from "../../../../models/Loan";

export default Method.get(`/mobile/Loan/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await Loan.findOne({ where: { id } 
  
  } );

  res.json(loan);
});