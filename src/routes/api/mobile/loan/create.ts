import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { boolean } from "joi";
import { AccessDeniedError, UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../../constants";
import { startTransaction } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { Loan } from "../../../../models/Loan";
import { LoanDetail } from "../../../../models/LoanDetail";
import { LoanPayGraph } from "../../../../models/LoanPayGraph"; 
import moment, { Moment } from "moment";
import { LoanTime } from "../../../../models/LoanTime";
import { Account } from "../../../../models/Account";
const schema = Joi.object({
  loanProductId    : Joi.string(),
  productId        : Joi.string(),
  loanTypeId       : Joi.string(),
  loanCategoryId   : Joi.string(),
  amount           : Joi.number().required(),
  customerId       : Joi.string().max(100).required(),
  loanDate         : Joi.date().required(),
  loanRate         : Joi.number().required(),
  currencyId       : Joi.string(),
  loanTimeId       : Joi.string().required(),
  isToZms          : Joi.boolean(),
  isJudge          : Joi.boolean(),
  dayCalcId        : Joi.string()
});

type IBody = {
  productId: string;
  loanProductId      : string;
  loanTypeId         : string;
  loanCategoryId     : string;
  amount             : number;
  customerId         : string;   
  loanDate           : Date;
  loanRate           : number;
  currencyId         : string;
  loanTimeId         : string;
  dayCalcId          : string;
  isToZms            : boolean;
  isJudge            : boolean;

} 

export default Method.post("/mobile/Loan/create", schema, async (req: Request, res: Response) => {
  const {
    amount,
    customerId,
    loanDate,
    loanRate,
    loanTimeId ,
 
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loan = await Loan.findOne({ where: { customerId : customerId }, transaction });
        // if (loan) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Хэрэглэгч бүртгэгдсэн байна.");
      
      const loanNew = new Loan({
        productId       : "eeab94b7-a98f-4592-a2a0-f9f33542cb51",
        loanProductId   : "2d063ff6-bd39-4938-97d0-74ba1fa33cb7",
        amount          : amount,
        customerId      : customerId,
        loanDate        : loanDate,
        loanRate        : loanRate,
        currencyId      : "a8b156e5-526b-46c9-bc02-547458546019",
        loanTimeId      : loanTimeId,
        loanStatusId      : "2"

      });

      const result =  await loanNew.save({ transaction });

      const account = await Account.findOne({ where: { customerId:customerId} });
      if (!account) throw new ValidationError(ERRORS.LOAN_DETAIL_NOTFOUND, "Зээлийн бүтээгдэхүүн олдсонгүй .");
   
      account.set({
        balance: account.balance - amount,
        loanAmount: account.loanAmount + amount,
      });

       await account.save({ transaction });
       
      const loanDetail = await LoanDetail.findOne({ where: { loanId : loanNew.id }, transaction });
      if (loanDetail) throw new ValidationError(ERRORS.LOAN_DETAIL_NOTFOUND, "Зээлийн бүтээгдэхүүн олдсонгүй .");
      
      const loanTime = await LoanTime.findOne({where: {id : loanTimeId},transaction});
      if(parseInt(loanTime?.name.match(/\d+/)?.[0] || '') < 4) {
        const loanTimeNumber = parseInt(loanTime?.name.match(/\d+/)?.[0] || '');

        const loanDetailNew = new LoanDetail({
        loanId        : loanNew.id,
        currencyId    : "a8b156e5-526b-46c9-bc02-547458546019",
        currencyRate  : loanRate,
        rate          : loanRate,
        amount        : amount,
        paymentDay1   : parseInt(moment(new Date (loanDate)).add(1,"month").format("DD")),
        startDate     : loanDate,
        endDate       : new Date(moment(new Date(loanDate)).add(loanTimeNumber,"month").format("YYYY-MM-DD")),
        loanPayTypeId :"1",
        loanPayFreqId :"1",
        loanFreePayCount:1,
        holidayCalcId :"1",

      });
      await loanDetailNew.save({ transaction });
      } else if (parseInt(loanTime?.name.match(/\d+/)?.[0] || '') > 4) {
        const loanTimeNumber = parseInt(loanTime?.name.match(/\d+/)?.[0] || '');
        const loanDetailNew = new LoanDetail({
        loanId        : loanNew.id,
        currencyId    : "a8b156e5-526b-46c9-bc02-547458546019",
        currencyRate  : loanRate,
        rate          : loanRate,
        amount        : amount,
        paymentDay1   : parseInt(moment(new Date (loanDate)).add(loanTimeNumber,"day").format("DD")),
        startDate     : loanDate,
        endDate       : new Date(moment(new Date(loanDate)).add(loanTimeNumber,"day").format("YYYY-MM-DD")),
        loanPayTypeId :"1",
        loanPayFreqId :"1",
        loanFreePayCount:1,
        holidayCalcId :"1",

      });
      await loanDetailNew.save({ transaction });
      }
      
      
        
    const loanpay = await LoanPayGraph.findOne({ where: { loanId : loanNew.id }, transaction });
    if (loanpay) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, " Бүртгэгдсэн байна.");  
    const loanTimeNumber = parseInt(loanTime?.name.match(/\d+/)?.[0] || '');    
    if(parseInt(loanTime?.name.match(/\d+/)?.[0] || '') < 4) {                     
      const paytimes1 = parseInt(moment(new Date(loanDate)).format("MM"));
      const paytimes2 = parseInt(moment(new Date(new Date(moment(new Date(loanDate)).add(loanTimeNumber,"month").format("YYYY-MM-DD")))).format("MM"));
      const paytime = paytimes2-paytimes1                                                       //-----------------------|
      // const diffRateDay = (moment.duration(moment((moment(new Date(loanDate)).add(1,"month").format("YYYY-MM-DD"))).diff(loanDate))).asDays() - diffDay;                                                                                       //-----------------------|
      let startMoment = moment(loanDate);                                                       //---------ЭХЛЭЛ---------|
      let endMoment = moment(moment(new Date(new Date(moment(new Date(loanDate)).add(loanTimeNumber,"month").format("YYYY-MM-DD")))));                                                          //-----------------------|
      let duration = moment.duration(endMoment.diff(startMoment));                              //_______________________|
      let days = duration.asDays();
      console.log("---------******days********",days);
      const rateDay = ((amount / 100 * loanRate) * paytime) / days;
      const now = moment(new Date()).format("YYYY-MM-DD");
      let diffDay = (moment.duration(moment((moment(new Date(loanDate)).add(1,"month").format("YYYY-MM-DD"))).diff(loanDate))).asDays()
      console.log("----------------diffday------***************",diffDay);
      let PaymentDay1 = new Date(moment(new Date(loanDate)).add(1,"month").format("YYYY-MM-DD"));
 
      let TotalPayment = amount+(((amount / 100 )* loanRate ) * paytime) ;
      let Payment = amount / paytime;

      for(let n = 1; n <= paytime; n++){
        
        const loanPayGraphNew = new LoanPayGraph ({
        loanId            : loanNew.id,
        payDate           : PaymentDay1,
        loanResidual      : TotalPayment - Payment - (diffDay * rateDay),
        totalPayAmount    : Payment + diffDay * rateDay,
        rateCalcDay       : diffDay,
        mainLoanPayAmount : Payment,
        rateAmount        : diffDay * rateDay
        
      });
      await loanPayGraphNew.save({ transaction });
      diffDay = (moment.duration(moment((moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD"))).diff(PaymentDay1))).asDays()
      TotalPayment -= (Payment + (diffDay * rateDay));
      PaymentDay1 = new Date(moment(new Date(PaymentDay1)).add(1,"month").format("YYYY-MM-DD"));
      
      console.log("***************totalpayment",TotalPayment);
      }        

    }  

        else if(parseInt(loanTime?.name.match(/\d+/)?.[0] || '') > 4){                                                                                           
          const rateDay = (amount / 100 * loanRate) 
          let PaymentDay1 = new Date(moment(new Date(loanDate)).add(loanTimeNumber,"day").format("YYYY-MM-DD"));
          
          const TotalPayment = amount + rateDay ;
            
            const loanPayGraphNew = new LoanPayGraph ({
            loanId            : loanNew.id,
            payDate           : PaymentDay1,
            loanResidual      : amount + rateDay,
            totalPayAmount    : amount + rateDay,
            rateCalcDay       : rateDay,
            mainLoanPayAmount : amount,
            rateAmount        : loanRate
            

          });
          await loanPayGraphNew.save({ transaction });

        }  
        
    

      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});