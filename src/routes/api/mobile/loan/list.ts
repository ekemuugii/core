import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { LoanPayGraph } from "../../../../models/LoanPayGraph";
import { Loan } from "../../../../models/Loan";
import { query } from "express";


export default Method.get(`/loan/listByCustomerId/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id;

  const { rows, count } = await Loan.findAndCountAll({
    where : {customerId : id},
    order : [["created_at", "DESC"]],

  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});


    