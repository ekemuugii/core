import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";

 import { ERRORS, USER_STATUS } from "../../../../constants";

import { Request, Response } from "../../../../middlewares/sign";
import { AddressType } from "../../../../models/AddressType";
import { CustomerAddress } from "../../../../models/CustomerAddress";
import { Province } from "../../../../models/Province";
import { District } from "../../../../models/District";
import { Khoroo } from "../../../../models/Khoroo";
export default Method.get(`/mobile/customerAddress/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const customerAddress = await CustomerAddress.findOne({ include : [ {
    model : AddressType,
    as : "addressType"
  },
  {
    model: Province,
    as   : "province"
  },
  {
    model: District,
    as    : "district",
  },
  {
    model: Khoroo,
    as   : "khoroo"
  }
] ,where: { id: id } });
  if (!customerAddress) throw new NotfoundError(ERRORS.CUSTOMER_ADDRESS_NOTFOUND, "Харилцагчийн хаяг олдсонгүй.");
 
  res.json(customerAddress);
});