import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../../middlewares/sign";
import { CustomerAddress } from "../../../../models/CustomerAddress";
import { startTransaction } from "../../../../core/sequelize";
const schema = Joi.object({
  customerId  : Joi.string().max(45).required(),
  addressTypeId : Joi.string().max(255).required(),
  provinceId   : Joi.string().max(100).required(),
  districtId: Joi.string().max(100).required(),
  khorooId   : Joi.string().max(100).required(),
  address     : Joi.string().max(255).required()
});
export default Method.post("/mobile/customerAddress/create", schema, async (req: Request, res: Response) => {
type IBody = {
  customerId : string
  addressTypeId : string
  provinceId : string
  districtId : string
  khorooId : string
  address : string
};
const { 
  customerId,
  addressTypeId,
  provinceId,
  districtId,
  khorooId,
  address
}: IBody = req.body;


  return startTransaction(async (commit, rollback, transaction) => {
    try { 
    const customerAddress = new CustomerAddress({
        customerId,
        addressTypeId,
        provinceId,
        districtId,
        khorooId,
        address

  });
  await customerAddress.save({ transaction });
      
  await commit();

  res.json({ success: true });
} catch (err) {
  await rollback();

  throw err;
}
});
});