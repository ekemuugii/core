import { Method } from "@goodtechsoft/xs-micro-service";

import { Request, Response } from "../../../../middlewares/sign";

import { Khoroo } from "../../../../models/Khoroo";
import { Province } from "../../../../models/Province";
import { District } from "../../../../models/District";
import { Gender } from "../../../../models/Gender";
import { Industry } from "../../../../models/Industry";
import { OrgType } from "../../../../models/OrgType";
import { Bank } from "../../../../models/Bank"
import { DayCalc } from "../../../../models/DayCalc"
import { LoanTime } from "../../../../models/LoanTime"
import { Currency } from "../../../../models/Currency";
import { WhoType } from "../../../../models/WhoType";
import { AddressType } from "../../../../models/AddressType";
import { MarriageStatus } from "../../../../models/MarriageStatus";
import { EducationType } from "../../../../models/EducationType";
import { EmployeeUnit } from "../../../../models/EmployeeUnit";
import { NationalityType } from "../../../../models/NationalityType";
import { WorkStatus } from "../../../../models/WorkStatus";
import { DepartmentUnit } from "../../../../models/DepartmentUnit";
import { LoanType } from "../../../../models/LoanType";
import { LoanProduct } from "../../../../models/LoanProduct";
import { LoanCategory } from "../../../../models/LoanCategory";
import { BorrowType } from "../../../../models/BorrowType";
import { LoanRateType } from "../../../../models/LoanRateType";
import { RateType } from "../../../../models/RateType";
import { Product } from "../../../../models/Product";
import { CapFreq } from "../../../../models/CapFreq";
import { AccrFreq } from "../../../../models/AccrFreq";
import { CalcType } from "../../../../models/CalcType";
import { HolidayCalc } from "../../../../models/HolidayCalc";
import { LoanPayType } from "../../../../models/LoanPayType";
import { LoanPayFreq } from "../../../../models/LoanPayFreq";
import { LoanStatus } from "../../../../models/LoanStatus";
import { LoanProductRate } from "../../../../models/LoanProductRate";

export default Method.get("/mobile/general/init", null, async (req: Request, res: Response) => {

  const provinces = await Province.findAll({ where: {} });
  const districts = await District.findAll({ where: {} });
  const khoroos = await Khoroo.findAll({ where: {} });
  const genders = await Gender.findAll({ where: {} });
  const industries = await Industry.findAll({ where: {} });
  const orgTypes = await OrgType.findAll({ where: {} });
  const banks = await Bank.findAll({ where: {} });
  const currencies = await Currency.findAll({ where: {} });
  const loanTimes = await LoanTime.findAll({ where: {} });
  const dayCalcs = await DayCalc.findAll({ where: {} });
  const whoTypes  = await WhoType.findAll({where : {}});
  const addressTypes  = await AddressType.findAll({where : {}});
  const marriageStatuses  = await MarriageStatus.findAll({where : {}});
  const educationTypes  = await EducationType.findAll({where : {}});
  const employeeUnits  = await EmployeeUnit.findAll({where : {}});
  const departmentUnits  = await DepartmentUnit.findAll({where : {}});
  const nationalityTypes  = await NationalityType.findAll({where : {}});
  const workStatus = await WorkStatus.findAll({where : {}} );
  const loanTypes = await LoanType.findAll({where : {}} );
  const loanProducts = await LoanProduct.findAll({where : {}} );
  const loanCategories = await LoanCategory.findAll({where : {}} );
  const borrowTypes = await BorrowType.findAll({where : {}} );
  const loanRateTypes = await LoanRateType.findAll({where : {}} );
  const rateTypes = await RateType.findAll({where : {}} );
  const products = await Product.findAll({where : {}} );
  const capFreqs = await CapFreq.findAll({where : {}} );
  const accrFreqs = await AccrFreq.findAll({where : {}} );
  const calcTypes = await CalcType.findAll({where : {}} );
  const holidayCalcs = await HolidayCalc.findAll({where : {}} );
  const loanPayTypes = await LoanPayType.findAll({where : {}} );
  const loanPayFreqs = await LoanPayFreq.findAll({where : {}} );
  const loanStatuses = await LoanStatus.findAll({where : {}} );
  const loanProductRate = await LoanProductRate.findOne({where : { loanProductId : "2d063ff6-bd39-4938-97d0-74ba1fa33cb7"}} );
  const rate = loanProductRate?.maxRate;
  res.json({
    provinces,
    districts,
    khoroos,
    genders,
    industries,
    orgTypes,
    banks,
    loanTimes,
    dayCalcs,
    currencies, 
    whoTypes,
    addressTypes,
    marriageStatuses,
    educationTypes,
    employeeUnits,
    nationalityTypes,
    workStatus,
    departmentUnits,
    loanTypes,
    loanProducts,
    loanCategories,
    borrowTypes,
    loanRateTypes,
    products,
    rateTypes,
    capFreqs,
    accrFreqs,
    calcTypes,
    holidayCalcs,
    loanPayTypes,
    loanPayFreqs,
    loanStatuses,
    rate
  });
});