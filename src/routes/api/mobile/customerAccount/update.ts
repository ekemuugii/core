import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { CustomerAccount } from "../../../../models/CustomerAccount";
import { startTransaction } from "../../../../core/sequelize";
      
      const schema = Joi.object({
        customerId      : Joi.string().max(100).required(),
        isOriginal      : Joi.number().max(100).required(),
        bankId      : Joi.string().max(100).required(),
        accountNumber   : Joi.number().max(100).required(),
      });
      
        type IBody = {
            customerId     : String;
            isOriginal     : Number ;
            bankId     : string ;
            accountNumber  : Number ;
      };
      
      export default Method.put(`/customer_account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
                customerId,
                isOriginal,
                bankId,
                accountNumber,
       
        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const  customerAccount = await CustomerAccount.findOne({ where: { id}, transaction });
          if (!customerAccount) throw new NotfoundError( "Харилцагчийн данс олдсонгүй.");
    
          customerAccount.set({
            customerId,
            bankId,
            accountNumber,
          });
      
          await customerAccount.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
    
          throw err;
        }
        });
      });
      