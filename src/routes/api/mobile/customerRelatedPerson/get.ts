import { Method,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../../constants";
import { Request, Response } from "../../../../middlewares/sign";
import { CustomerRelatedPerson } from "../../../../models/CustomerRelatedPerson";
import { WhoType } from "../../../../models/WhoType";

export default Method.get(`/mobile/customerRelatedPerson/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const customerRelatedPerson = await CustomerRelatedPerson.findOne({ include : [ {
    model : WhoType,
    as : "whoType"
  }
] ,where: { id } });
   
  if (!customerRelatedPerson) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Хэрэглэгч олдсонгүй.");
  res.json(customerRelatedPerson);
});