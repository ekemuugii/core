import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../../middlewares/sign";
import { CustomerRelatedPerson } from "../../../../models/CustomerRelatedPerson";
import { startTransaction } from "../../../../core/sequelize";
const schema = Joi.object({
 
  customerId   : Joi.string().max(45).required(),
  whoTypeId      : Joi.string().max(255).required(),
  firstName    : Joi.string().max(100).required(),
  lastName     : Joi.string().max(100).required(),
  phone        : Joi.string().max(100).required(),
});
  
  type IBody = {
    customerId : string;
    whoTypeId : string;
    firstName : string;
    lastName : string;
    phone : string;
  }

export default Method.post("/mobile/customerRelatedPerson", schema, async (req: Request, res: Response) => {
  const{
    customerId,
    whoTypeId,
    firstName,
    lastName,
    phone
  }: IBody = req.body;
  return startTransaction(async (commit, rollback, transaction) => {
    try {
 

      const customerRelatedPerson = new CustomerRelatedPerson({
        customerId,
        whoTypeId,
        firstName,
        lastName,
        phone
      });
      
 await customerRelatedPerson.save({ transaction });
  
  
  await commit();
  res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});