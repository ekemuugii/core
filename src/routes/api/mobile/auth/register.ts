// import { kafkaWrapper, AuthRegisteredProducer } from "@goodtechsoft/cb-broker";
import { Method, UnauthorizedError } from "@goodtechsoft/xs-micro-service";
import { v4 as uuidv4 } from "uuid";
import { Request, Response } from "../../../../middlewares/sign";
import Joi from "joi";

const schema = Joi.object({
  email   : Joi.string().max(255).required(),
  password: Joi.string().max(255).required()
});

type IBody = {
  email: string;
  password: string;
}

export default Method.post("/login", schema, async (req: Request, res: Response) => {
  const { 
    email,
    password
  }: IBody = req.body;

  const USER_ID = uuidv4();

  // new AuthRegisteredProducer(kafkaWrapper).send(USER_ID, {
  //   id       : USER_ID,
  //   sessionId: USER_ID
  // });
});