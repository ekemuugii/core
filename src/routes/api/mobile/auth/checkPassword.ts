// import { kafkaWrapper, UserAuthChangedProducer } from "@goodtechsoft/cb-broker";
import { Method, UnauthorizedError } from "@goodtechsoft/xs-micro-service";
import bcrypt from "bcryptjs";
import Joi from "joi";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../../../config";
import { ATTEMPT_STATUS, ERRORS } from "../../../../constants";
import { Request, Response, signIn } from "../../../../middlewares/sign";
import { Attempt } from "../../../../models/Attempt";
import { Session } from "../../../../models/Session";
import { User } from "../../../../models/User";
import email from "../../../../config/email";
import { ValidationError } from "sequelize";

const regexError = new Error ("Рэгистерийн дугаар буруу байна.")

const schema = Joi.object({
  password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).required(),
  id: Joi.string().required()
});

type IBody = {
  password: string;
  id: string;
}

export default Method.post("/mobile/auth/checkPassword", schema, async (req: Request, res: Response) => {

  const { 
    password,
    id 
  }: IBody = req.body;
  try {
        const user = await User.findOne({
            where: {
              id: id
            }
          });
          if (!user) throw new UnauthorizedError(ERRORS.AUTHENTICATION_FAILED);
        const check =  await user.validatePassword(password) 

      res.json(check);
      } catch (err) {
        throw err;
      }
    });