import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../../middlewares/sign";
import { DepartmentUnit } from "../../../../models/DepartmentUnit";
import { EmployeeUnit } from "../../../../models/EmployeeUnit";
import { Role } from "../../../../models/Role";
import { User } from "../../../../models/User";
import { Customer } from "../../../../models/Customer";
import { Staff } from "../../../../models/Staff";

export default Method.get("/mobile/auth/me", null, async (req: Request, res: Response) => {
  
  const user = await User.findOne({
    where: {
      id: req.user!.id
    },
    // include: [ {
    //   model: DepartmentUnit,
    //   as   : "departmentUnit"
    // }, {
    //   model: EmployeeUnit,
    //   as   : "employeeUnit"
    // }, {
    //   model: Role,
    //   as   : "role"
    // }]
  });

  const regUser = await User.findOne({
    where: {
      id: user!.id
    },
    attributes: ["lastName", "firstName"]
  });
  
  const customer = await Customer.findAll({ 
    where: {
      id: user?.customerId
    }

  });

  const staff = await Staff.findAll({ 
    where: {
      id: user?.staffId
    }

  });
  
  res.json({
    ...user!.toJSON(),
    pin    : undefined,
    customer: {
      createdAt      : undefined,
      refCode        : undefined,
      regUserId      : undefined,
      ownerUserId    : undefined,
      isConfirmed    : undefined,
      partnerCategory: undefined
    },
    regUser      : regUser || {},

  });
});