import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../../constants";
import { startTransaction } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { LoanPayGraph } from "../../../../models/LoanPayGraph";

const schema = Joi.object({
    loanId         : Joi.string().max(250).required(),
    payDate        : Joi.date().required(),
    loanResidual   : Joi.number().required(),
    totalPayAmount       : Joi.number().required(),
    rateCalcDay        : Joi.number().required(),
    mainLoanPayAmount   : Joi.number().required(),
    rateAmount   : Joi.number().required(),
});

type IBody = {
   loanId           : string;
   payDate          : Date;
   loanResidual     : number;
   totalPayAmount         : number;
   rateCalcDay          : number;
   mainLoanPayAmount     : number;
   rateAmount     : number;
} 

export default Method.post("/mobile/LoanPayGraph/create", schema, async (req: Request, res: Response) => {
  const {
    loanId,
    payDate,
    loanResidual,
    totalPayAmount,
    rateCalcDay,        
    mainLoanPayAmount,  
    rateAmount
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        const loanpay = await LoanPayGraph.findOne({ where: { loanId : loanId }, transaction });
        if (loanpay) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, " Бүртгэгдсэн байна.");

      const loanPayGraphNew = new LoanPayGraph({
        loanId        : loanId,
        payDate       : payDate,
        loanResidual  : loanResidual,
        totalPayAmount      : totalPayAmount,
        rateCalcDay       : rateCalcDay,
        mainLoanPayAmount  : mainLoanPayAmount,
        rateAmount : rateAmount

      });

      const result =  await loanPayGraphNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});