import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { date, number } from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../../constants";
import { startTransaction } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { Customer } from "../../../../models/Customer";
import bcrypt from "bcryptjs";
import { User } from "../../../../models/User";
import { Account } from "../../../../models/Account";

const schema = Joi.object({
  firstName: Joi.string().max(100).required(),
  lastName: Joi.string().max(100).required(),
  familyName: Joi.string(),
  birthDate: Joi.date().max(100).required(),
  registerNo: Joi.string().regex(/^[А-Я]{2}\d{8}$/).required(),
  phone: Joi.string().required(),
  email: Joi.string().regex(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).required(),
  signature: Joi.string().max(100),
  educationType: Joi.string().max(100),
  marriageStatus: Joi.string().max(100),
  familyCount: Joi.number(),
  incomeAmountMonth: Joi.number(),
  avatar: Joi.string().max(100),
  nationalityType: Joi.string().max(100),
  genderId: Joi.string().max(100),
  birthPlace: Joi.string().max(100),
  birthPlaceNote: Joi.string().max(100),
  password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).required()
});

type IBody = {
  firstName         : string;
  lastName          : string;
  familyName        : string;
  birthDate         : Date;
  registerNo        : string;
  phone             : string;
  email             : string;
  signature         : string;
  educationType     : string;
  marriageStatus    : string;
  familyCount       : number;
  incomeAmountMonth : number;
  avatar            : string;
  nationalityType   : string;
  genderId          : string;
  birthPlace        : string;
  birthPlaceNote    : string;
  userId            : string;
  password          : string;
}

export default Method.post("/mobile/customer/create", schema, async (req: Request, res: Response) => {
  console.log(req.body);
  const {
    firstName,
    lastName,
    registerNo,
    phone,
    email,
    genderId,
    password,
    birthDate
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const customer = await Customer.findOne({ where :{registerNo: registerNo}, transaction }); 
      if (customer) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Харилцагч регистр бүртгэгдсэн байна.")

  const SALT_ROUNDS = 10;
  const newPassword = await bcrypt.hash(password, SALT_ROUNDS);



      const customerNew = new Customer({  
        firstName          : firstName,
        lastName           : lastName,
        registerNo         : registerNo,
        phone              : phone,
        email              : email,
        genderId           : genderId,
        birthDate          : birthDate
        
      });
      
      const userNew = new User({  
        firstName          : firstName,
        lastName           : lastName,
        phone              : phone,
        email              : email,
        password           : newPassword,
        username           : phone,
        userStatus         : phone,
        customerId        : customerNew.id,
        userStatusDate     : new Date("2022-06-05"),
      });
      await userNew.save({ transaction });

      const Score = 500000;
      const account = new Account({
        accountNumber :   Math.random() * (100000000 - 10000000) + 10000000,
        name          : registerNo,
        customerId    : customerNew.id,
        balance       : Score,
        scoreAmount   : Score,
        loanAmount    : 0
      });


      await account.save({ transaction });
      
      const result = await customerNew.save({ transaction });
  
      await commit();

      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});