import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { Customer } from "../../../../models/Customer";
import { startTransaction } from "../../../../core/sequelize";
      
      const schema = Joi.object({
        firstName          : Joi.string(),
        lastName           : Joi.string(),
        familyName         : Joi.string().max(100).required,
        birthDate          : Joi.date(),
        registerNo         : Joi.string(),
        phone              : Joi.string(),
        email              : Joi.string(),
        signature          : Joi.string().max(100).required(),
        educationTypeId    : Joi.string().max(100).required(),
        marriageStatusId   : Joi.string().max(100).required(),
        familyCount        : Joi.number().max(100).required(),
        incomeAmountMonth  : Joi.number().max(100).required(),
        avatar             : Joi.string().max(100).required(),
        nationalityTypeId  : Joi.string().max(100).required(),
        genderId           : Joi.string().max(100).required(),
        birthPlace         : Joi.string().max(100).required(),
        birthPlaceNote     : Joi.string().max(100).required(),
        workStatusId       : Joi.string().max(100).required(),
      });
      
        type IBody = {
            firstName         : string;
            lastName          : string;
            familyName        : string;
            birthDate         : Date;
            registerNo        : string;
            phone             : string;
            email             : string;
            signature         : string;
            educationTypeId    : string;
            marriageStatusId   : string;
            familyCount       : Number;
            incomeAmountMonth : Number;
            avatar            : string;
            nationalityTypeId   : string;
            genderId          : string;
            birthPlace        : string;
            birthPlaceNote     : string;
            workStatusId      : string;
      };
      
      export default Method.put(`/mobile/customer/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
            familyName,
            signature,
            educationTypeId,
            marriageStatusId,
            familyCount,
            incomeAmountMonth,
            avatar,
            nationalityTypeId,
            genderId,
            birthPlace,
            birthPlaceNote,
            workStatusId,
        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const customer = await Customer.findOne({ where: { id}, transaction });
          if (!customer) throw new NotfoundError( "Харилцагч олдсонгүй.");
    
          customer.set({
            familyName,
            signature,
            educationTypeId,
            marriageStatusId,
            familyCount,
            incomeAmountMonth,
            avatar,
            nationalityTypeId,
            genderId,
            birthPlace,
            birthPlaceNote,
            workStatusId
          });
      
          const result =  await customer.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
          throw err;
        }
        });
      });
      