import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../../middlewares/sign";
import { Customer } from "../../../../models/Customer";
import { startTransaction } from "../../../../core/sequelize";
import { ERRORS } from "../../../../constants";

type IBody = {
    id : string;
}
export default Method.get(`/mobile/customer/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id

  const customer = await Customer.findOne({ where: { id } }); 
  if (!customer) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    res.json({
      result: customer
    });
});
    
