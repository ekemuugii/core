import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../../middlewares/sign";
import { Account } from "../../../../models/Account";

export default Method.get(`/mobile/account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;
console.log(id);
  const account = await Account.findOne({ where: { customerId :id } });
  console.log(account);
  res.json(account);
});