import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../../constants";
import { Request, Response } from "../../../../middlewares/sign";
import { LoanDetail } from "../../../../models/LoanDetail";
    

export default Method.del(`/mobile/Loan_detail/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await LoanDetail.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.LOAN_DETAIL_NOTFOUND, " Зээлийн бүтээгдэхүүн олдсонгүй. ");
    
    await LoanDetail.destroy({ where: { id} });
    
    res.json({ success: true });
});
