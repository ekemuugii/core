import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { LoanDetail } from "../../../../models/LoanDetail";

const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/mobile/LoanDetail/list", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query;
    
    filters[Op.or] = [
    //   Sequelize.where(Sequelize.col("product_type_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("loan_type_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("loan_category_id"), "LIKE", `%${query}%`),
    //   Sequelize.where(Sequelize.col("amount"), "LIKE", `%${query}%`),
      Sequelize.where(Sequelize.col("loan_id"), "LIKE", `%${query}%`),
      Sequelize.where(Sequelize.col("id"), "LIKE", `%${query}%`),


    ];
  }

  const { rows, count } = await LoanDetail.findAndCountAll({
    where : filters,
    // include: [{
    //   model: User,
    //   as   : "customer"
    // }],
    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});


    