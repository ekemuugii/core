import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { number } from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../../constants";
import { startTransaction } from "../../../../core/sequelize";
import { Request, Response } from "../../../../middlewares/sign";
import { LoanDetail } from "../../../../models/LoanDetail";
import { LoanPayGraph } from "../../../../models/LoanPayGraph";
import { Loan } from "../../../../models/Loan";
import moment from "moment";


const schema = Joi.object({
    loanId           : Joi.string().max(250).required(),
    currencyId       : Joi.string(),
    currencyRate     : Joi.number(),
    rate             : Joi.number(),
    amount           : Joi.number(),
    paymentDay1      : Joi.number().max(31).required(),
    paymentDay2      : Joi.number().max(31).optional().allow(null, ""),
    startDate        : Joi.date().required(),
    endDate          : Joi.date().required(),
    loanPayTypeId    : Joi.string(),
    loanPayFreqId    : Joi.string(),
    loanFreePayCount : Joi.number(),
    holidayCalcId    :Joi.string(),


});

type IBody = {
   loanId           : string;
   currencyId       : string;
   currencyRate     : number;
   rate             : number;
   paymentDay1      : number;
   paymentDay2      : number;
   startDate        : Date;
   endDate          : Date;
   amount           : number;
//    totalResidual    : number;
   loanPayTypeId    : string;
   loanPayFreqId    : string;
   loanFreePayCount : number;

   holidayCalcId  :string;
   loanRate : number;
   
} 

export default Method.post("/Loan_detail/create", schema, async (req: Request, res: Response) => {
  const {
    loanId,
    paymentDay1,
    startDate,
    endDate,
    amount,
    // totalResidual,



  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        const loanDetail = await LoanDetail.findOne({ where: { loanId : loanId }, transaction });
        if (loanDetail) throw new ValidationError(ERRORS.LOAN_DETAIL_NOTFOUND, "Зээлийн бүтээгдэхүүн олдсонгүй .");

      const loanDetailNew = new LoanDetail({
        loanId,
        paymentDay1,
        startDate,
        endDate,
        amount,
      });


      await loanDetailNew.save({ transaction });


      await commit();
 
      res.json({ success: true });
    } catch (err) {
      await rollback();
 
      throw err;
    }
  });
});

// const start = moment(new Date()).add(1, "months").toISOString();

// const future = moment(new Date(start)).add(4, "months");//.format("YYYY-MM-DD");
// console.log(" >>> future >>>", future);

// const now = moment(new Date());//.format("YYYY-MM-DD");
// console.log(" >>> now >>>", now);

// const diff = moment(future).diff(now);
// console.log(" >>> diff >>>", diff);

// const duration = moment.duration(diff).asDays();
// console.log(" >>> duration >>>", duration);
        