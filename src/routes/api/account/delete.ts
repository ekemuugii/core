import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Account } from "../../../models/Account";
    

export default Method.del(`/account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await Account.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Данс олдсонгүй.");
    
    await Account.destroy({ where: { id } });
    
    res.json({ success: true });
});
