import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { Account } from "../../../models/Account";

export default Method.get(`/account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await Account.findOne({ where: { customerId :id } });

  res.json(loan);
});