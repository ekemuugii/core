import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import Joi from "joi";
import { Transaction } from "../../../models/Transaction";

const Schema = Joi.object({
  type                 : Joi.string().max(45).required(),
  transactionStatus    : Joi.string().max(45).required(),
  paymentId            : Joi.string().max(45).required(),
  paymentMethod        : Joi.string().max(45).required(),
  payerUserId          : Joi.string().max(45).required(),
  creditAccountBank    : Joi.string().max(45).required(),
  creditAccountId      : Joi.string().max(45).required(),
  creditAccountNumber  : Joi.string().max(45).required(),
  creditAccountCurrency: Joi.string().max(45).required(),
  creditAccountName    : Joi.string().max(45).required(),
  debitAccountId       : Joi.string().max(45).required(),
  debitAccountBank     : Joi.string().max(45).required(),
  debitAccountNumber   : Joi.string().max(45).required(),
  debitAccountName     : Joi.string().max(45).required(),
  debitAccountCurrency : Joi.string().max(45).required(),
  description          : Joi.string().max(45).required(),
  addInfo              : Joi.string().max(45).required(),
  amount               : Joi.string().max(45).required(),
  hasTrxFee            : Joi.boolean().required(),
  trxFee               : Joi.number().required(),
  hasPaymentFee        : Joi.boolean().required(),
  paymentFee           : Joi.number().required(),
  hasBankTrxFee        : Joi.boolean().required(),
  bankTrxFee           : Joi.number().required(),
  totalAmount          : Joi.number().required(),
  tranId               : Joi.string().max(45).required(),
  tranDate             : Joi.string().max(45).required(),
  tranStatus           : Joi.string().max(45).required()
});

  type IBody = {
    type : string;
    transactionStatus: string;
    paymentId: string;
    paymentMethod: string;
    payerUserId: string;
    creditAccountBank: string;
    creditAccountId: string;
    creditAccountNumber: string;
    creditAccountCurrency: string;
    creditAccountName: string;
    debitAccountId: string;
    debitAccountBank: string;
    debitAccountNumber: string;
    debitAccountName: string;
    debitAccountCurrency: string;
    description: string;
    addInfo: string;
    amount: string;
    hasTrxFee: boolean;
    trxFee: number;
    hasPaymentFee: boolean;
    paymentFee: number;
    hasBankTrxFee: boolean;
    bankTrxFee: number;
    totalAmount: number;
    tranId: string;
    tranDate: string;
    tranStatus: string;
  }
export default Method.put(`/transaction/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    type,
    transactionStatus,
    paymentId,
    paymentMethod,
    payerUserId,
    creditAccountBank,
    creditAccountId,
    creditAccountNumber,
    creditAccountCurrency,
    creditAccountName,
    debitAccountId,
    debitAccountBank,
    debitAccountNumber,
    debitAccountName,
    debitAccountCurrency,
    description,
    addInfo,
    amount,
    hasTrxFee,
    trxFee,
    hasPaymentFee,
    paymentFee,
    hasBankTrxFee,
    bankTrxFee,
    totalAmount,
    tranId,
    tranDate,
    tranStatus     
  }: IBody = req.body;
  console.log(req.body);
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const glTransaction1 = await Transaction.findOne({ where: { id }, transaction });
      if (!glTransaction1) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагчийн  файл олдсонгүй.");
          
      glTransaction1.set({
        type,
        transactionStatus,
        paymentId,
        paymentMethod,
        payerUserId,
        creditAccountBank,
        creditAccountId,
        creditAccountNumber,
        creditAccountCurrency,
        creditAccountName,
        debitAccountId,
        debitAccountBank,
        debitAccountNumber,
        debitAccountName,
        debitAccountCurrency,
        description,
        addInfo,
        amount,
        hasTrxFee,
        trxFee,
        hasPaymentFee,
        paymentFee,
        hasBankTrxFee,
        bankTrxFee,
        totalAmount,
        tranId,
        tranDate,
        tranStatus  
      });
      const result = await glTransaction1.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});