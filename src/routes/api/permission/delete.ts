import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Permission } from "../../../models/Permission";

export default Method.del(`/permission/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;


  const permission = await Permission.findOne({ where: { id } });
  if (!permission) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны мэдээлэл олдсонгүй.");


  await Permission.destroy({ where: { id: permission.id } });
 
  res.json({ success: true });
});