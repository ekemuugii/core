import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Permission } from "../../../models/Permission";

const schema = Joi.object({
  roleId    : Joi.string().max(45).required(),
  refCode   : Joi.string().max(45).required(),
  isRead    : Joi.date().optional().allow(null, ""),
  isEdit    : Joi.date().optional().allow(null, ""),
  isDelete  : Joi.date().optional().allow(null, ""),
});

type IBody = {
  roleId    : string;
  refCode   : string;
  isRead    : boolean;
  isEdit    : boolean;
  isDelete  : boolean;
}

export default Method.post("/permission/create", schema, async (req: Request, res: Response) => {
  const {
    roleId,
    refCode,
    // isRead,
    // isEdit,
    // isDelete
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        const permission = await Permission.findOne({ where :{roleId: roleId}, transaction }); 
        if (permission) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, " Бүртгэгдсэн байна.");

        const permissionNew = new Permission({
            roleId,
            refCode,
            // isRead,
            // isEdit,
            // isDelete
    });
      
  await permissionNew.save();

  res.json( permissionNew );
} catch (err) {
  await rollback();

  throw err;
}
});
});