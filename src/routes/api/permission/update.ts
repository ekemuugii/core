import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Permission } from "../../../models/Permission";
import Joi from "joi";

const Schema = Joi.object({
    roleId    : Joi.string().max(45).required(),
    refCode   : Joi.string().max(45).required(),
    isRead    : Joi.boolean().required(),
    isEdit    : Joi.boolean().required(),
    isDelete  : Joi.boolean().required()
});

  type IBody = {
    roleId   : string;
    refCode  : string;
    isRead   : boolean;
    isEdit   : boolean;
    isDelete : boolean;
  }
export default Method.put(`/permission/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    roleId,
    refCode,
    isRead,
    isEdit,
    isDelete      
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const permission = await Permission.findOne({ where: { id }, transaction });
      if (!permission) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны файл олдсонгүй.");
          
      permission.set({
        roleId,
        refCode,
        isRead,
        isEdit,
        isDelete    
      });
      const result = await permission.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});