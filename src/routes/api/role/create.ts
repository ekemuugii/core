import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { Role } from "../../../models/Role";
import { startTransaction } from "../../../core/sequelize";

const schema = Joi.object({
  name        : Joi.string().max(45).required(),
  roleStatus  : Joi.string().max(100).required()
});

type IBody = {
  name       : string;
  roleStatus : string;
}

export default Method.post("/role/create", schema, async (req: Request, res: Response) => {
  const {
    name,
    roleStatus
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {

  const role = new Role({
    name,
    roleStatus
  });
      
  await role.save();

  res.json( role );
} catch (err) {
  await rollback();

  throw err;
}
});
});