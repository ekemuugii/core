import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Role } from "../../../models/Role";
import Joi from "joi";

const Schema = Joi.object({
  name        : Joi.string().max(100).required(),
  roleStatus  : Joi.string().max(100).required()
});

  type IBody = {
    name : string;
    roleStatus : string;
  }
export default Method.put(`/role/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    name,
    roleStatus      
  }: IBody = req.body;
  console.log(req.body);
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const role = await Role.findOne({ where: { id }, transaction });
      if (!role) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны  мэдээлэл олдсонгүй.");
          
      role.set({
        name,
        roleStatus     
      });
      const result = await role.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});