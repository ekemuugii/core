import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Role } from "../../../models/Role";

export default Method.get(`/role/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const role = await Role.findOne({ where: { id } });
  if (!role) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны мэдээлэл олдсонгүй.");
  res.json(role);
});