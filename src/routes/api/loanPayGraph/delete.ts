import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { LoanPayGraph } from "../../../models/LoanPayGraph";
    

export default Method.del(`/LoanPayGraph/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await LoanPayGraph.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await LoanPayGraph.destroy({ where: { id} });
    
    res.json({ success: true });
});
