import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanPayGraph } from "../../../models/LoanPayGraph";

export default Method.get(`/LoanPayGraph/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanPayGraph.findOne({ where: { id } });

  res.json(loan);
});