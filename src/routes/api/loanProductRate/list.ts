import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductRate } from "../../../models/LoanProductRate";
import { LoanProduct } from "../../../models/LoanProduct";
import { CalcType } from "../../../models/CalcType";
import { AccrFreq } from "../../../models/AccrFreq";
import { CapFreq } from "../../../models/CapFreq";
import { LoanRateType } from "../../../models/LoanRateType";

const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/LoanProductRate/list", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query;
    
    filters[Op.or] = [
      Sequelize.where(Sequelize.col("loan_product_id"), "like", `%${query}%`),
    ];
  }

  const { rows, count } = await LoanProductRate.findAndCountAll({
    where : filters,
    include: [{
      model: LoanProduct,
      as   : "loanProduct"
    },
    {
      model: CalcType,
      as   : "calcType"
    },
    {
      model: AccrFreq,
      as   : "accrFreq"
    },
    {
      model: CapFreq,
      as   : "capFreq"
    },
    {
      model: LoanRateType,
      as   : "loanRateType"
    }],
    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});