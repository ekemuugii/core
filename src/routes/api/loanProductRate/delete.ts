import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductRate } from "../../../models/LoanProductRate";

export default Method.del(`/LoanProductRate/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const loan = await LoanProductRate.findOne({ where: { id } });
  if (!loan) throw new NotfoundError(ERRORS.LOAN_PRODUCT_NOTFOUND, "ЗЭЭЛИЙН БҮТЭЭГДХҮҮНИЙ ХҮҮ ОЛДСОНГҮЙ.");
    
  await LoanProductRate.destroy({ where: { id } });
    
  res.json({ success: true });
});