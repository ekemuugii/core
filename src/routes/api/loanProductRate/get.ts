import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductRate } from "../../../models/LoanProductRate";

export default Method.get(`/LoanProductRate/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanProductRate.findOne({ where: { id } });

  res.json(loan);
});