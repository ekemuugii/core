import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProductRate } from "../../../models/LoanProductRate";

const schema = Joi.object({
  loanProductId: Joi.string().max(255).required(),
  loanRateTypeId: Joi.string().max(255).required(),
  calcTypeId: Joi.string().max(255).required(),
  minRate: Joi.number().required(),
  maxRate: Joi.number().required(),
  accrFreqId: Joi.string().max(255).required(),
  capFreqId: Joi.string().max(255).required(),

});

type IBody = {
  loanProductId : string;
  loanRateTypeId : string;
  calcTypeId : string;
  minRate : number;
  maxRate : number;
  capFreqId : string;
  accrFreqId : string;

} 

export default Method.post("/LoanProductRate/create", schema, async (req: Request, res: Response) => {
  const {
    loanProductId,
    loanRateTypeId,
    calcTypeId,
    minRate,
    maxRate,
    capFreqId,
    accrFreqId
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loanProductRate = await LoanProductRate.findOne({ where: { loanProductId: loanProductId }, transaction });
      if (loanProductRate) throw new ValidationError(ERRORS.LOAN_PRODUCT_ALREADY_REGISTERED, "ЗЭЭЛИЙН ХҮҮНИЙ ТӨРӨЛ БҮРТГЭГДСЭН БАЙНА.");

      const loanProductRateNew = new LoanProductRate({
        loanProductId: loanProductId,
        loanRateTypeId: loanRateTypeId,
        calcTypeId: calcTypeId,
        minRate: minRate,
        maxRate: maxRate,
        capFreqId: capFreqId,
        accrFreqId: accrFreqId,
      });

      const result = await loanProductRateNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();
 
      throw err;
    }
  });
});