import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { BlackList } from "../../../models/BlackList";
import { LoanCoBorrower } from "../../../models/LoanCoBorrower";
    

export default Method.del(`/loanCoBorrower/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loanCoBorrower = await LoanCoBorrower.findOne({ where: { id } });
    if (!loanCoBorrower) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await LoanCoBorrower.destroy({ where: { id} });
    
    res.json({ success: true });
});
