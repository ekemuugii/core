import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import Joi, { custom } from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { BlackList } from "../../../models/BlackList";
import { startTransaction } from "../../../core/sequelize";
import { Customer } from "../../../models/Customer";
import { LoanCoBorrower } from "../../../models/LoanCoBorrower";

const schema = Joi.object({
  customerId   : Joi.string().max(45).required(),
  loanId   : Joi.string().max(45).required(),
  whoTypeId   : Joi.string().max(45).required(),
  borrowTypeId   : Joi.string().max(45).required(),
});
  
  type IBody = {
    customerId : string;
    loanId : string;
    isActive   : boolean;
    whoTypeId : string;
    borrowTypeId : string;

  }

export default Method.post("/loanCoBorrower/create", schema, async (req: Request, res: Response) => {
  const{
    customerId,
    loanId,
    whoTypeId,
    borrowTypeId,
    isActive

  }: IBody = req.body;
  return startTransaction(async (commit, rollback, transaction) => {
    try {

      const loanCoBorrower = new LoanCoBorrower({
        customerId,
        loanId,
        isActive,
        whoTypeId,
        borrowTypeId,

      });
      
 await loanCoBorrower.save({ transaction });
  
  await commit();
  res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});