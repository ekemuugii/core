import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
import { addAttribute } from "sequelize-typescript";
import { LoanCoBorrower } from "../../../models/LoanCoBorrower";
import { BorrowType } from "../../../models/BorrowType";
import { WhoType } from "../../../models/WhoType";

const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/loanCoBorrower/list", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query;
    
    filters[Op.or] = [
      Sequelize.where(Sequelize.col("loan_id"), "like", `%${query}%`),
    ];
  }

  const { rows, count } = await LoanCoBorrower.findAndCountAll({
    where : filters,
    include: [{
      model: Customer,
      as   : "customer",
      attributes : ["id","first_name","last_name", "register_no", "phone"  ]
    },
    {
      model: BorrowType,
      as   : "borrowType"
     
    },
    {
      model: WhoType,
      as   : "whoType"
     
    }], 
    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});
