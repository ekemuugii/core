import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanCoBorrower } from "../../../models/LoanCoBorrower";

export default Method.get(`/loanCoBorrower/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanCoBorrower.findOne({ where: { id } });

  res.json(loan);
});