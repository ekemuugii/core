import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { Staff } from "../../../models/Staff";
import { startTransaction } from "../../../core/sequelize";
      
      const schema = Joi.object({
        registerNo       : Joi.string().max(100).required(),
        familyName       : Joi.string().max(100).required(),
        firstName        : Joi.string().max(100).required(),
        lastName         : Joi.string().max(100).required(),
        avatar           : Joi.string().max(100).required(),
        roleId           : Joi.string().max(100).required(),
        departmentUnitId : Joi.string().max(100).required(),
        employeeUnitId   : Joi.string().max(100).required(),
        roleEnableDate   : Joi.date().required(),
        roleDisableDate  : Joi.date().required(),
        phone            : Joi.string().max(100).required(),
        email            : Joi.string().max(100).required(),
        userStatus       : Joi.string().max(100).required(),
        userId           : Joi.string().max(100).required(),
      });
      
        type IBody = {
            registerNo        : string;
            familyName        : string;
            firstName         : string;
            lastName          : string;
            avatar            : string;
            roleId            : string;
            departmentUnitId  : string;
            employeeUnitId    : string;
            roleEnableDate    : Date;
            roleDisableDate   : Date;
            phone             : string;
            email             : string;
            userStatus        : string;
            userId            : string;

      };
      
      export default Method.put(`/staff/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
            registerNo,
            familyName,
            firstName,
            lastName,
            avatar,
            roleId,
            departmentUnitId,
            employeeUnitId,
            roleEnableDate,
            roleDisableDate,
            phone,
            email,
            userStatus,
            userId,
        
        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const staff = await Staff.findOne({ where: { id}, transaction });
          if (!staff) throw new NotfoundError( "Харилцагч олдсонгүй.");
    
          staff.set({
            registerNo,
            familyName,
            firstName,
            lastName,
            avatar,
            roleId,
            departmentUnitId,
            employeeUnitId,
            roleEnableDate,
            roleDisableDate,
            phone,
            email,
            userStatus
          });
      
          const result =  await staff.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
          throw err;
        }
        });
      });
      