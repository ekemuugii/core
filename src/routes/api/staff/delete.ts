import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Staff } from "../../../models/Staff";
    

export default Method.del(`/staff/:${Method.uuid("id")}`,null, async (req: Request, res: Response) => {
  const id: string = req.params.id;   
  const staff = await Staff.findOne({ where: { id } });

  if (!staff) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    await Staff.destroy({ where: { id: staff.id } });

  res.json({ success: true });
});

