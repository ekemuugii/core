import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Staff } from "../../../models/Staff";
import bcrypt from "bcryptjs";
import { User } from "../../../models/User";

const schema = Joi.object({
  registerNo: Joi.string().regex(/^[А-Яа-я|Үү|Өө|Ёё]{2}\d{8}$/).required(),
  familyName: Joi.string().max(100).required(),
  firstName: Joi.string().max(100).required(),
  lastName: Joi.string().max(100).required(),
  // avatar: Joi.string().max(100).required(),
  roleId: Joi.string().max(100).required(),
  departmentUnitId: Joi.string().max(100).required(),
  employeeUnitId: Joi.string().max(100).required(),
  roleEnableDate: Joi.date().required(),
  roleDisableDate: Joi.date().required(),
  phone: Joi.string().max(100).required(),
  email: Joi.string().regex(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).required(),
  userStatus: Joi.string().max(100).required(),

  
});

type IBody = {
  registerNo       : string;
  familyName       : string;
  firstName        : string;
  lastName         : string;
  avatar           : string;
  roleId           : string;
  departmentUnitId : string;
  employeeUnitId   : string;
  roleEnableDate   : Date;
  roleDisableDate  : Date;
  phone            : string;
  email            : string;
  userStatus       : string;

}

export default Method.post("/staff/create", schema, async (req: Request, res: Response) => {
  const {
    registerNo,
    familyName,
    firstName,
    lastName,
    avatar,
    roleId,
    departmentUnitId,
    employeeUnitId,
    roleEnableDate,
    roleDisableDate,
    phone,
    email,
    userStatus
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const staff = await Staff.findOne({ where :{registerNo: registerNo}, transaction }); 
      if (staff) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, " Регистр бүртгэгдсэн байна.");
      
      const SALT_ROUNDS = 10;
      const password = "Swast1ka*";
      const newPassword = await bcrypt.hash(password, SALT_ROUNDS);

      const staffNew = new Staff({  
        registerNo        : registerNo,
        familyName        : familyName,
        firstName         : firstName,
        lastName          : lastName,
        avatar            : avatar,
        roleId            : roleId,
        departmentUnitId  : departmentUnitId,
        employeeUnitId    : employeeUnitId,
        roleEnableDate    : roleEnableDate,
        roleDisableDate   : roleDisableDate,
        phone             : phone,
        email             : email,
        userStatus        : userStatus

        
      });

      const userNew = new User({  
        firstName          : firstName,
        lastName           : lastName,
        phone              : phone,
        email              : email,
        password           : newPassword,
        username           : phone,
        userStatus         : phone,
        userStatusDate     : new Date(),
        staffId            : staffNew.id
      });
      await userNew.save({ transaction });
 
      const result =  await staffNew.save({ transaction });
  
  
      await commit();

      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});