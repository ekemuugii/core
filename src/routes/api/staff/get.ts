import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { Staff } from "../../../models/Staff";
import { startTransaction } from "../../../core/sequelize";
import { ERRORS } from "../../../constants";

type IBody = {
    id : string;
}
export default Method.get(`/staff/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id

  const staff = await Staff.findOne({ where: { id :id } }); 
  if (!staff) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтан олдсонгүй!!!.");
    res.json({
      result: staff
    });
});
    