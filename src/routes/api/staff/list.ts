import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Staff } from "../../../models/Staff";
import { EmployeeUnit } from "../../../models/EmployeeUnit";
import { DepartmentUnit } from "../../../models/DepartmentUnit";

const schema = Joi.object({
  filter: Joi.object({ 
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/staff/list", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

 
  const filters: IFilter = {};
  
  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query.toLowerCase();
    
    filters[Op.or] = [
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("register_no")), "LIKE", `%${query}%`),
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("first_name")), "LIKE", `%${query}%`),
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("last_name")), "LIKE", `%${query}%`),
      Sequelize.where(Sequelize.col("phone"), "=", `%${query}%`),
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("email")), "LIKE", `%${query}%`),
    ];
  }

  const { rows, count } = await Staff.findAndCountAll({
    where : filters,
    include : [{
      model: EmployeeUnit,
      as : "employeeUnit"
    },
    {
      model : DepartmentUnit,
      as    : "departmentUnit"
    }],
    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});

    
