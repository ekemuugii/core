import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi, { date, number } from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
import bcrypt from "bcryptjs";
import { User } from "../../../models/User";
import { Account } from "../../../models/Account";

const schema = Joi.object({
  firstName: Joi.string().max(100).required(),
  lastName: Joi.string().max(100).required(),
  familyName: Joi.string().max(100).required(),
  birthDate: Joi.date().required(),
  registerNo: Joi.string().regex(/^[А-Яа-я|Үү|Өө|Ёё]{2}\d{8}$/).required(),
  phone: Joi.string().required(),
  email: Joi.string().regex(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).required(),
  signature: Joi.string().max(100).required(),
  educationTypeId: Joi.string().max(100).required(),
  marriageStatusId: Joi.string().max(100).required(),
  familyCount: Joi.number().required(),
  incomeAmountMonth: Joi.number().required(),
  avatar: Joi.string().max(100).required(),
  nationalityTypeId: Joi.string().max(100).required(),
  genderId: Joi.string().max(100).required(),
  birthPlace: Joi.string().max(100).required(),
  birthPlaceNote: Joi.string().max(100).required(),
  workStatusId : Joi.string().max(100).required(),
});

type IBody = {
  firstName         : string;
  lastName          : string;
  familyName        : string;
  birthDate         : Date;
  registerNo        : string;
  phone             : string;
  email             : string;
  signature         : string;
  educationTypeId   : string;
  marriageStatusId  : string;
  familyCount       : number;
  incomeAmountMonth : number;
  avatar            : string;
  nationalityTypeId : string;
  genderId          : string;
  birthPlace        : string;
  birthPlaceNote    : string;
  userId            : string;
  workStatusId      : string;
}

export default Method.post("/customer/create", schema, async (req: Request, res: Response) => {
  console.log(req.body);
  const {
    firstName,
    lastName,
    familyName,
    birthDate,
    registerNo,
    phone,
    email,
    signature,
    educationTypeId,
    marriageStatusId,
    familyCount,
    incomeAmountMonth,
    avatar,
    nationalityTypeId,
    genderId,
    birthPlace,
    birthPlaceNote,
    workStatusId,
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const customer = await Customer.findOne({ where :{registerNo: registerNo}, transaction }); 
      if (customer) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Харилцагч регистр бүртгэгдсэн байна.")

  const SALT_ROUNDS = 10;
  const password = "Swast1ka*";
  const newPassword = await bcrypt.hash(password, SALT_ROUNDS);


      const customerNew = new Customer({  
        firstName          : firstName,
        lastName           : lastName,
        familyName         : familyName,
        birthDate          : birthDate,
        registerNo         : registerNo,
        phone              : phone,
        email              : email,
        signature          : signature,
        educationTypeId    : educationTypeId,
        marriageStatusId   : marriageStatusId,
        familyCount        : familyCount,
        incomeAmountMonth  : incomeAmountMonth,
        avatar             : avatar,
        nationalityTypeId  : nationalityTypeId,
        genderId           : genderId,
        birthPlace         : birthPlace,
        birthPlaceNote     : birthPlaceNote,
        workStatusId       : workStatusId
        
      });
      
      const userNew = new User({  
        firstName          : firstName,
        lastName           : lastName,
        phone              : phone,
        email              : email,
        password           : newPassword,
        username           : phone,
        userStatus          : "active",
        customerId          : customerNew.id,
        userStatusDate      : new Date("2022-05-29"),
      });
      await userNew.save({ transaction });
  
      const Score = 500000;
      const account = new Account({
        accountNumber :   Math.random() * (100000000 - 10000000) + 10000000,
        name          : registerNo,
        customerId    : customerNew.id,
        balance       : Score,
        scoreAmount   : Score,
        loanAmount    : 0

      });

      await account.save({ transaction });
      
      const result = await customerNew.save({ transaction });
  
      await commit();

      res.json({ success: result });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});