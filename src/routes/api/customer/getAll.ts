import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
import { startTransaction } from "../../../core/sequelize";
import { CustomerAccount } from "../../../models/CustomerAccount";
import { CustomerAddress } from "../../../models/CustomerAddress";
import { CustomerMedia } from "../../../models/CustomerMedia";
import { CustomerRelatedPerson } from "../../../models/CustomerRelatedPerson";

type IBody = {
    id : string;
}
export default Method.get(`/customer/getAll/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;
  const getCustomer = await Customer.findOne({where: { id } }); 
  const getCustomerAccount = await CustomerAccount.findOne({ where: { customerId: id } }); 
  const getCustomerAddress = await CustomerAddress.findOne({ where: { customerId: id } }); 
  const getCustomerRelatedPerson = await CustomerRelatedPerson.findOne({ where: { customerId: id } }); 
  const getCustomerMedia = await CustomerMedia.findOne({ where: { customerId: id } }); 
  

  const result = {
    customer: getCustomer,
    customerAccount: getCustomerAccount ,
    ustomerAddress:getCustomerAddress,
    customerRelatedPerson:getCustomerRelatedPerson,
    customerMedia:getCustomerMedia
  }
    res.json(result);
});
    