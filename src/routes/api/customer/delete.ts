import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
    

export default Method.del(`/customer/:${Method.uuid("id")}`,null, async (req: Request, res: Response) => {
  const id: string = req.params.id;   
  const customer = await Customer.findOne({ where: { id } });

  if (!customer) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    await Customer.destroy({ where: { id: customer.id } });

  res.json({ success: true });
});

