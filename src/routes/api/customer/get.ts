import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { Customer } from "../../../models/Customer";
import { startTransaction } from "../../../core/sequelize";
import { ERRORS } from "../../../constants";
import { EducationType } from "../../../models/EducationType";
import { MarriageStatus } from "../../../models/MarriageStatus";
import { NationalityType } from "../../../models/NationalityType";
import { Gender } from "../../../models/Gender";
import { WorkStatus } from "../../../models/WorkStatus";

type IBody = {
    id : string;
}
export default Method.get(`/customer/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id = req.params.id

  const customer = await Customer.findOne({ 
    include : [ {
    model : EducationType,
    as : "educationType"
  },
  {
    model : MarriageStatus,
    as : "marriageStatus"
  },
  {
    model : NationalityType,
    as : "nationalityType"
  },
  {
    model : Gender,
    as : "gender"
  },
  {
    model : WorkStatus,
    as    : "workStatus"
  }
],where: { id } }); 
  if (!customer) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    res.json({
      result: customer
    });
});
    