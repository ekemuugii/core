// import { kafkaWrapper, UserAuthEndedProducer } from "@goodtechsoft/cb-broker";
import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response, signOut } from "../../../middlewares/sign";

export default Method.get("/logout", null, async (req: Request, res: Response) => {

  // new UserAuthEndedProducer(kafkaWrapper).send(req.user!.id, {
  //   id: req.user!.id
  // });
  
  signOut(res);
});