// import { kafkaWrapper, UserAuthChangedProducer } from "@goodtechsoft/cb-broker";
import { Method, UnauthorizedError } from "@goodtechsoft/xs-micro-service";
import bcrypt from "bcryptjs";
import Joi from "joi";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../../config";
import { ATTEMPT_STATUS, ERRORS } from "../../../constants";
import { Request, Response, signIn } from "../../../middlewares/sign";
import { Attempt } from "../../../models/Attempt";
import { Session } from "../../../models/Session";
import { User } from "../../../models/User";
import email from "../../../config/email";
import { ValidationError } from "sequelize";

const regexError = new Error ("Рэгистэрийн дугаар буруу байна.")

const schema = Joi.object({
  email   : Joi.string().regex(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).required(),
  password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/).required()
});

type IBody = {
  email: string;
  password: string;
}

const EXPIRES_HOURS = 24;
const SALT_ROUNDS = 10;

export default Method.post("/login", schema, async (req: Request, res: Response) => {
  const { 
    email,
    password
  }: IBody = req.body;
  const SALT_ROUNDS = 10;

    const newPassword = await bcrypt.hash(password, SALT_ROUNDS);
  const user = await User.findOne({
    where: {
      email, 
      isActive: true
    }
  });

  if (!user) throw new UnauthorizedError(ERRORS.AUTHENTICATION_FAILED);
  if (user.passwordExpired === true) throw new UnauthorizedError(ERRORS.PASSWORD_EXPIRED, "Password expired!");

  if (user.userSuspended) throw new UnauthorizedError(ERRORS.USER_SUSPENDED, "User suspended!");
  
  if (user.userTerminated) throw new UnauthorizedError(ERRORS.USER_TERMINATED, "User terminated!");

  let STAFF;
  

  // if (!STAFF) throw new UnauthorizedError(ERRORS.AUTHENTICATION_FAILED, "Нэвтрэх нэр эсвэл нууц үг буруу байна!");
  
  let SESSION_ID = uuidv4();

  if (req.sessionID && req.useragent) {
    let SESSION = await Session.findOne({ 
      where: {
        sessionID: req.sessionID
      }
    });

    if (!SESSION) {
      SESSION = new Session({
        sessionID  : req.sessionID,
        deviceToken: req.deviceToken!,
        deviceImei : req.deviceImei!,
        isMobile   : req.useragent.isMobile,
        isDesktop  : req.useragent.isDesktop,
        isBot      : `${req.useragent.isBot}` === "postman",
        browser    : req.useragent.browser,
        os         : req.useragent.os,
        platform   : req.useragent.platform,
        source     : req.useragent.source,
        version    : req.useragent.version
      });
    }
   
    SESSION.set({
      deviceToken: req.deviceToken!
    });
    await SESSION.save();
    SESSION_ID = SESSION.id;
  }    

  try {
    if (!(await user.validatePassword(password))) throw new UnauthorizedError(ERRORS.AUTHENTICATION_FAILED);

    const HOURS = (EXPIRES_HOURS + 4) * (60 * 60);
    const EXPIRES_IN = Math.floor(Date.now() / 1000) + HOURS;
    const SESSION_SECRET = await bcrypt.hash(`${SESSION_ID}`, SALT_ROUNDS);

    await new Attempt({
      userId           : user.id,
      sessionId        : SESSION_ID,
      ipAddress        : req.useragent?.geoIp.ipAddress,
      attemptStatus    : ATTEMPT_STATUS.SUCCESS,
      attemptStatusDate: new Date()
    }).save();

    user.set({
      expiryHours: EXPIRES_HOURS,
      sessionId :SESSION_ID
    });
    await user.save();

    const refreshToken = jwt.sign({
      uid: user.id,
      sid: SESSION_SECRET
    }, config.jwt.apiSecret, { expiresIn: EXPIRES_IN });

    // new UserAuthChangedProducer(kafkaWrapper).send(user.id, {
    //   id               : user.id,
    //   sessionId        : SESSION_ID,
    // });
    
    signIn(res, { 
      user, 
      refreshToken, 
      expiresIn : EXPIRES_IN, 
      sessionId : SESSION_ID.toString(),

  
    });
  } catch (err) {

    await new Attempt({
      userId           : user.id,
      sessionId        : SESSION_ID,
      ipAddress        : req.useragent?.geoIp.ipAddress,
      attemptStatus    : ATTEMPT_STATUS.FAILED,
      attemptStatusDate: new Date()
    }).save();

    throw err;
  }
});