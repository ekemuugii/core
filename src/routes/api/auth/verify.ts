// import { kafkaWrapper, UserEmailVerifiedProducer } from "@goodtechsoft/cb-broker";
import { Method, UnauthorizedError } from "@goodtechsoft/xs-micro-service";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";
import { config } from "../../../config";
import { ERRORS } from "../../../constants";
import { SESSION_SCOPE } from "../../../constants/SESSION_SCOPE";
import { Request, Response, signIn } from "../../../middlewares/sign";
import { Session } from "../../../models/Session";
import { User } from "../../../models/User";

const EXPIRES_HOURS = 24;
const SALT_ROUNDS = 10;

export default Method.get("/:verifyId/verify", null, async (req: Request, res: Response) => {
  const verifyId: string = req.params.verifyId;

  const user = await User.findOne({ where: { id: verifyId } });
  if (!user) throw new UnauthorizedError(ERRORS.AUTHENTICATION_FAILED, "Хүсэлт амжилтгүй.");

  // const duration = moment.duration(moment(user.verifyExpiryDate).add(3, "minute").diff(moment(new Date())));
  // if (duration.asSeconds() <= 0) throw new ValidationError(ERRORS.INVALID_ACTION, "Бүртгэл баталгаажуулах кодны хүчинтэй хугацаа дуссан байна.");

  // user.set({
  //   verifyId        : null,
  //   verifyExpiryDate: null
  // });
  // await user.save();

  let SESSION_ID = uuidv4();

  if (req.sessionID && req.useragent) {
    let SESSION = await Session.findOne({
      where: {
        sessionID: req.sessionID
      }
    });

    if (!SESSION) {
      SESSION = new Session({
        sessionID  : req.sessionID,
        deviceToken: req.deviceToken!,
        deviceImei : req.deviceImei!,
        isMobile   : req.useragent.isMobile,
        isDesktop  : req.useragent.isDesktop,
        isBot      : `${req.useragent.isBot}` === "postman",
        browser    : req.useragent.browser,
        os         : req.useragent.os,
        platform   : req.useragent.platform,
        source     : req.useragent.source,
        version    : req.useragent.version
      });
    }

    SESSION.set({
      deviceToken: req.deviceToken!
    });

    await SESSION.save();

    SESSION_ID = SESSION.id;
  }

  const HOURS = (EXPIRES_HOURS + 4) * (60 * 60);
  const EXPIRES_IN = Math.floor(Date.now() / 1000) + HOURS;
  const SESSION_SECRET = await bcrypt.hash(`${SESSION_ID}`, SALT_ROUNDS);

  user.set({
    expiryHours: EXPIRES_HOURS
  });
  await user.save();

  // if (!user.isEmailVerified) {
  //   user.set({
  //     isEmailVerified  : true,
  //     emailVerifiedDate: new Date()
  //   });
    
  //   // new UserEmailVerifiedProducer(kafkaWrapper).send(user.id, {
  //   //   id               : user.id,
  //   //   updatedAt        : new Date().toISOString(),
  //   //   isEmailVerified  : true,
  //   //   emailVerifiedDate: user.emailVerifiedDate.toISOString()
  //   // });
  // }
  
  const refreshToken = jwt.sign({
    uid: user.id,
    sid: SESSION_SECRET
  }, config.jwt.apiSecret, { expiresIn: EXPIRES_IN });

  signIn(res, {
    user,
    refreshToken,
    expiresIn: EXPIRES_IN,
    sessionId: SESSION_ID.toString()
  }, SESSION_SCOPE.FORGOT);
});