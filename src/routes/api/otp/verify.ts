// import { kafkaWrapper, UserEmailVerifiedProducer } from "@goodtechsoft/cb-broker";
import { Method, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { v4 as uuidv4 } from "uuid";
import { ERRORS, OTP_METHOD, OTP_STATUS } from "../../../constants";
import { SESSION_SCOPE } from "../../../constants/SESSION_SCOPE";
import { Request, Response, signIn } from "../../../middlewares/sign";
import { Otp } from "../../../models/Otp";

const schema = Joi.object({
  otpMethod: Joi.string().valid(OTP_METHOD.FORGOT, OTP_METHOD.REGISTER).required(),
  otpCode  : Joi.string().max(100).required()
});

type IBody = {
  otpMethod: string;
  otpCode: string;
}

export default Method.post("/otp/verify", schema, async (req: Request, res: Response) => {
  const {
    otpMethod,
    otpCode
  }: IBody = req.body;

  const otp = await Otp.findOne({ 
    where: {
      otpMethod,
      userId   : req.user!.id, 
      otpStatus: OTP_STATUS.NEW
    },
    order: [["createdAt", "DESC"]]
  });

  if (!otp) throw new ValidationError(ERRORS.INVALID_OTP_CODE, "Баталгаажуулах код буруу байна!");

  if (!await otp.validateCode(otpCode))
    throw new ValidationError(ERRORS.INVALID_OTP_CODE, "Баталгаажуулах код буруу байна!");

  if (!await otp.validateExpiryIn())
    throw new ValidationError(ERRORS.OTP_CODE_EXPIRE_IN, "Баталгаажуулах кодны идэвхтэй хугацаа дууссан байна!");

  otp.set({
    otpStatus    : OTP_STATUS.SUCCESS,
    otpStatusDate: new Date()
  });
  await otp.save();

  console.log("req.user verify otp ", req.user);

  await Otp.update({ 
    otpStatus    : OTP_STATUS.EXPIRED, 
    otpStatusDate: new Date() 
  }, { 
    where: {
      userId   : req.user!.id, 
      otpStatus: OTP_STATUS.NEW  
    }
  });

  // if (!req.user!.isPhoneVerified) {
  //   // new UserEmailVerifiedProducer(kafkaWrapper).send(req.user!.id, {
  //   //   id               : req.user!.id,
  //   //   updatedAt        : new Date().toISOString(),
  //   //   isEmailVerified  : true,
  //   //   emailVerifiedDate: new Date().toISOString()
  //   // });
  // }
  
  signIn(res, {
    user     : req.user!,
    sessionId: uuidv4()
  }, SESSION_SCOPE.CHANGE_PASSWORD);
});