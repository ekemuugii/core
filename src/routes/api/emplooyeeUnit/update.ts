import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { EmployeeUnit } from "../../../models/EmployeeUnit";
import Joi from "joi";

const Schema = Joi.object({
    name              : Joi.string().max(100).required(),
    departmentUnitId  : Joi.string().max(45).required(),
});

  type IBody = {
    name             : string;
    departmentUnitId : string;
  }

export default Method.put(`/employee_unit/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    name,
    departmentUnitId
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const employeeUnit = await EmployeeUnit.findOne({ where: { id }, transaction });
      if (!employeeUnit) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны файл олдсонгүй.");
          
      employeeUnit.set({
        name,
        departmentUnitId
      });
      const result = await employeeUnit.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});