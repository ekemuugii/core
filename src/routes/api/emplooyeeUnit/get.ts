import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { EmployeeUnit } from "../../../models/EmployeeUnit";

export default Method.get(`/employee_unit/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const employeeUnit = await EmployeeUnit.findOne({ where: { id } });
  if (!employeeUnit) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны мэдээлэл олдсонгүй.");
  res.json(employeeUnit);
});