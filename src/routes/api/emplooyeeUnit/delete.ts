import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { EmployeeUnit } from "../../../models/EmployeeUnit";

export default Method.del(`/employee_unit/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;


  const employeeUnit = await EmployeeUnit.findOne({ where: { id } });
  if (!employeeUnit) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны мэдээлэл олдсонгүй.");


  await EmployeeUnit.destroy({ where: { id: employeeUnit.id } });
 
  res.json({ success: true });
});