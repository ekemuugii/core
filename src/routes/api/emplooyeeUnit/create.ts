import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { EmployeeUnit } from "../../../models/EmployeeUnit";
import { startTransaction } from "../../../core/sequelize";

const schema = Joi.object({
  name              : Joi.string().max(100).required(),
  departmentUnitId  : Joi.string().max(45).required()
});

type IBody = {
  name             : string;
  departmentUnitId : string;
}

export default Method.post("/employee_unit/create", schema, async (req: Request, res: Response) => {
  const {
    name,
    departmentUnitId
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {

  const employeeUnit = new EmployeeUnit({
    name,
    departmentUnitId
  });
      
  await employeeUnit.save();

  res.json( employeeUnit );
} catch (err) {
  await rollback();

  throw err;
}
});
});