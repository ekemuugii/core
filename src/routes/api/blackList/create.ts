import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import Joi, { custom } from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { BlackList } from "../../../models/BlackList";
import { startTransaction } from "../../../core/sequelize";
import { Customer } from "../../../models/Customer";

const schema = Joi.object({
  customerId   : Joi.string().max(45).required(),
  isActive     : Joi.boolean().required(),

});
  
  type IBody = {
    customerId : string;
    isActive   : boolean;

  }

export default Method.post("/Black_list/create", schema, async (req: Request, res: Response) => {
  const{
    customerId,
    isActive

  }: IBody = req.body;
  return startTransaction(async (commit, rollback, transaction) => {
    try {

      const blacklist = new BlackList({
        customerId,
        isActive

      });
      
 await blacklist.save({ transaction });
  
  await commit();
  res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});