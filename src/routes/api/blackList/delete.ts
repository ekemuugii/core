import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { BlackList } from "../../../models/BlackList";
    

export default Method.del(`/Black_list/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const blacklist = await BlackList.findOne({ where: { id } });
    if (!blacklist) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await BlackList.destroy({ where: { id} });
    
    res.json({ success: true });
});
