import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { PaymentDetail } from "../../../models/PaymentDetail";

const Schema = Joi.object({
  amount       : Joi.number().required(),
  currency     : Joi.string().max(45).required(),
  description  : Joi.string().max(45).required(),
  paymentStatus: Joi.string().max(45).required(),
  payerUserId  : Joi.string().max(45).required(),
  paidDate     : Joi.date().required()
});

  type IBody = {
    amount: number;
    currency: string;
    description: string;
    paymentStatus: string;
    payerUserId: string;
    paidDate : Date;
  }
export default Method.put(`/paymentDetail/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id: string = req.params.id;

  const {
    amount,
    currency,
    description,
    paymentStatus,
    payerUserId,
    paidDate
   
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        
      const paymentDetail = await PaymentDetail.findOne({ where: { id }, transaction });
      if (!paymentDetail) throw new NotfoundError(ERRORS.CUSTOMER_RELATED_PERSON_NOTFOUND, "ТӨЛБӨРИЙН МЭДЭЭЛЭЛ ОЛДСОНГҮЙ.");
        
      paymentDetail.set({
        amount,
        currency,
        description,
        paymentStatus,
        payerUserId,
        paidDate
      });
      const result = await paymentDetail.save({ transaction });
        
      await commit();
  
      res.json({ success: true });
    } catch (err) {
      await rollback();
  
      throw err;
    }
  });
});