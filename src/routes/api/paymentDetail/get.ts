import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { PaymentDetail } from "../../../models/PaymentDetail";

export default Method.get(`/paymentDetail/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const paymentDetail = await PaymentDetail.findOne({ where: { id } });
   
  if (!paymentDetail) throw new NotfoundError(ERRORS.CUSTOMER_RELATED_PERSON_NOTFOUND, "ТӨЛБӨРИЙН МЭДЭЭЛЭЛ ОЛДСОНГҮЙ.");
  res.json(paymentDetail);
});