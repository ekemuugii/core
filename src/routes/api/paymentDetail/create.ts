import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { PaymentDetail } from "../../../models/PaymentDetail";
import { startTransaction } from "../../../core/sequelize";
const schema = Joi.object({
  amount       : Joi.number().required(),
  loanId       : Joi.number().required(),
  // currency     : Joi.string().max(45).required(),
  // description  : Joi.string().max(250).required(),
  // paymentStatus: Joi.string().max(45).required(),
  payerUserId  : Joi.string().max(45).required(),
  paidDate     : Joi.date().required()
});
  
  type IBody = {
    amount: number;
    loanId : string;
    // currency: string;
    // description: string;
    // paymentStatus: string;
    payerUserId: string;
    paidDate : Date;
  }

export default Method.post("/PaymentDetail/create", schema, async (req: Request, res: Response) => {
  const {
    amount,
    loanId,
    // currency,
    // description,
    // paymentStatus,
    payerUserId,
    paidDate
  }: IBody = req.body;
  return startTransaction(async (commit, rollback, transaction) => {
    try {

      const paymentDetail = new PaymentDetail({
        amount,
        loanId,
        // currency,
        // description,
        // paymentStatus,
        payerUserId,
        paidDate
      });

      await commit();
      res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});