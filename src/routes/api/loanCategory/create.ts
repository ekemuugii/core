import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanCategory } from "../../../models/LoanCategory";

const schema = Joi.object({
    name       : Joi.string().max(255).required(),
    parentId   : Joi.string().max(255).required(),
    // isActive   : Joi.boolean().required(),

});

type IBody = {
   name       : string;
   parentId   : string;
//  isActive   : boolean;

} 

export default Method.post("/Loan_category/create", schema, async (req: Request, res: Response) => {
  const {
    name,
    parentId,
    // isActive,
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loan = await LoanCategory.findOne({ where: { parentId : parentId }, transaction });
        if (loan) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Хэрэглэгч бүртгэгдсэн байна.");

      const loanNew = new LoanCategory({
        name      : name,
        parentId  : parentId,
        // isActive  : isActive,

      });

      const result =  await loanNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();

 
      throw err;
    }
  });
});