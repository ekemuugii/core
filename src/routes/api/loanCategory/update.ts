import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { LoanCategory } from "../../../models/LoanCategory";
import { startTransaction } from "../../../core/sequelize";
      
      const schema = Joi.object({
        name       : Joi.string().max(255).required(),
        parentId   : Joi.string().max(255).required(),
      });
      
        type IBody = {
            name        : string;
            parentId    : string ;

      };
      
      export default Method.put(`/Loan_category/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
            name,
            parentId,

        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const  loancategory = await LoanCategory.findOne({ where: { id}, transaction });
          if (!loancategory) throw new NotfoundError( "Харилцагч олдсонгүй.");
    
          loancategory.set({
            name,
            parentId,
          });
      
          await loancategory.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
    
          throw err;
        }
        });
      });
      