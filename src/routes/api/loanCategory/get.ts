import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanCategory } from "../../../models/LoanCategory";

export default Method.get(`/Loan_category/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanCategory.findOne({ where: { id } });

  res.json(loan);
});