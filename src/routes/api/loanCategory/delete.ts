import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { LoanCategory } from "../../../models/LoanCategory";
    

export default Method.del(`/Loan_category/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await LoanCategory.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Категор олдсонгүй.");
    
    await LoanCategory.destroy({ where: { id} });
    
    res.json({ success: true });
});
