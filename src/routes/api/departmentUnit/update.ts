import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { DepartmentUnit } from "../../../models/DepartmentUnit";
import Joi from "joi";

const Schema = Joi.object({
    name      : Joi.string().max(100).required(),
    parentId  : Joi.string().max(45).required(),
});

  type IBody = {
    name     : string;
    parentId : string;
  }

export default Method.put(`/department_unit/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    name,
    parentId
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const departmentUnit = await DepartmentUnit.findOne({ where: { id }, transaction });
      if (!departmentUnit) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны файл олдсонгүй.");
          
      departmentUnit.set({
        name,
        parentId
      });
      const result = await departmentUnit.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});