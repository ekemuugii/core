import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { DepartmentUnit } from "../../../models/DepartmentUnit";
import { startTransaction } from "../../../core/sequelize";

const schema = Joi.object({
  name      : Joi.string().max(100).required(),
  parentId  : Joi.string().max(45).required()
});

type IBody = {
  name     : string;
  parentId : string;
}

export default Method.post("/department_unit/create", schema, async (req: Request, res: Response) => {
  const {
    name,
    parentId
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {

  const departmentUnit = new DepartmentUnit({
    name,
    parentId
  });
      
  await departmentUnit.save();

  res.json( departmentUnit );
} catch (err) {
  await rollback();

  throw err;
}
});
});