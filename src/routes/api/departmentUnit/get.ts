import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { DepartmentUnit } from "../../../models/DepartmentUnit";

export default Method.get(`/department_unit/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const departmentUnit = await DepartmentUnit.findOne({ where: { id } });
  if (!departmentUnit) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Ажилтаны мэдээлэл олдсонгүй.");
  res.json(departmentUnit);
});