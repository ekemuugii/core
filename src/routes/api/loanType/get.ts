import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanType } from "../../../models/LoanType";
export default Method.get(`/LoanType/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanType.findOne({ where: { id } });

  res.json(loan);
});