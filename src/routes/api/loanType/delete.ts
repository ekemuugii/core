import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { LoanType } from "../../../models/LoanType";    

export default Method.del(`/LoanType/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const loan = await LoanType.findOne({ where: { id } });
    if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await LoanType.destroy({ where: { id } });
    
    res.json({ success: true });
});
