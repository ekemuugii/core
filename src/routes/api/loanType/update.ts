import { Method ,NotfoundError } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import Joi from "joi";
import { startTransaction } from "../../../core/sequelize";
import { LoanType } from "../../../models/LoanType";
      
      const schema = Joi.object({
        name       : Joi.string().max(255).required(),
        isActive   : Joi.boolean().required(),
      });
      
        type IBody = {
            name        : string;
            isActive    : boolean ;

      };
      
      export default Method.put(`/LoanType/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
        const id: string = req.params.id;
        const {
            name,
            isActive,

        }: IBody = req.body;
      
        return startTransaction(async (commit, rollback, transaction) => {
          try {
          const  loan = await LoanType.findOne({ where: { id}, transaction });
          if (!loan) throw new NotfoundError( "Харилцагч олдсонгүй.");
    
          loan.set({
            name,
            isActive,
          });
      
          await loan.save({ transaction });
          await commit();
          res.json({ success: true });
        } catch (err) {
          await rollback();
    
          throw err;
        }
        });
      });
      