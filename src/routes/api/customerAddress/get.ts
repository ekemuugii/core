import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";

 import { ERRORS, USER_STATUS } from "../../../constants";

import { Request, Response } from "../../../middlewares/sign";
import { AddressType } from "../../../models/AddressType";
import { CustomerAddress } from "../../../models/CustomerAddress";

export default Method.get(`/customer_address/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const customerAddress = await CustomerAddress.findOne({ include : [ {
    model : AddressType,
    as : "addressType"
  }
] ,where: { id: id } });
  if (!customerAddress) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагчийн хаяг олдсонгүй.");
 
  res.json(customerAddress);
});