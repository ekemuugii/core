import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAddress } from "../../../models/CustomerAddress";
import Joi from "joi";
 
const Schema = Joi.object({
  addressTypeId : Joi.string().max(100).required(),
  provinceId   : Joi.string().max(100).optional().allow(null, ""),
  districtId: Joi.string().max(100).optional().allow(null, ""),
  khorooId   : Joi.string().max(100).optional().allow(null, ""),
  address     : Joi.string().max(255).required()
});

  type IBody = {
    addressTypeId : string;
    provinceId : string;
    districtId : string;
    khorooId : string;
    address : string;
  }

export default Method.put(`/customer_address/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id: string = req.params.id;
  const {
    addressTypeId,
    provinceId,
    districtId,
    khorooId,
    address       
  }: IBody = req.body;
  console.log(req.body);
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        
      const customerAddress = await CustomerAddress.findOne({ where: { id }, transaction });
      if (!customerAddress) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Хэрэглэгч бүртгэлгүй байна.");
        
      customerAddress.set({
    
        addressTypeId,
        provinceId,
        districtId,
        khorooId,
        address      
      });
      const result = await customerAddress.save({ transaction });
        
      await commit();
  
      res.json({ success: result });
    } catch (err) {
      await rollback();
      throw err;
    }
  });
});