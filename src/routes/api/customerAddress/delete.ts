import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAddress } from "../../../models/CustomerAddress";
import { ERRORS } from "../../../constants";

// export default Method.post("/customer_address/delete", null, async (req: Request, res: Response) => {
//   const id: string = req.body.id;

export default Method.del(`/customer_address/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;

  const customerAddress = await CustomerAddress.findOne({ where: { id } });


  if (!customerAddress) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагчийн хаяг олдсонгүй");
  
  await CustomerAddress.destroy({ where: { id } });
  res.json({ success: true });
});