import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAddress } from "../../../models/CustomerAddress";
import { Province } from "../../../models/Province";
import { District } from "../../../models/District";
import { Khoroo } from "../../../models/Khoroo";
import { AddressType } from "../../../models/AddressType";

const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/customer_address", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query.toLowerCase();
    filters[Op.or] = [
      Sequelize.where( Sequelize.col("customer_id"), "=", `${query}`),
    ];
  }

  if (filter.query2 && filter.query2 !== "" && filter.query2 !== null) {
    const query2 = filter.query2.toLowerCase();
    filters[Op.or] = [
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("district_id")), "LIKE", `%${query2}%`)
     
    ];
  }
  
  const { rows, count } = await CustomerAddress.findAndCountAll({
    where : filters,
    include: [{
      model: Province,
      as   : "province"
    },
    {
      model: District,
      as    : "district",
    },
    {
      model: Khoroo,
      as   : "khoroo"
    },
    {
      model : AddressType,
      as    : "addressType"
    }],
    order : [["created_at", "DESC"]],
    offset: parseInt(offset.page, 10) -1,
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});