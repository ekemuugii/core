import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerMedia } from "../../../models/CustomerMedia";
import Joi from "joi";

const Schema = Joi.object({
  customerId: Joi.string().max(100).required(),
  name      : Joi.string().max(100).required(),
  url       : Joi.string().max(100).required()
});

  type IBody = {
    customerId : string;
    name : string;
    url : string;
  }
export default Method.put(`/customer_media/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id:string = req.params.id;

  const {
    customerId,
    name,
    url      
  }: IBody = req.body;
  console.log(req.body);
  return startTransaction(async (commit, rollback, transaction) => {
    try {
          
      const customerMedia = await CustomerMedia.findOne({ where: { id }, transaction });
      if (!customerMedia) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагчийн  файл олдсонгүй.");
          
      customerMedia.set({
        customerId,
        name,
        url     
      });
      const result = await customerMedia.save({ transaction });
          
      await commit();
    
      res.json({ success: result });
    } catch (err) {
      await rollback();
    
      throw err;
    }
  });
});