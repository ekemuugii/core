import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerMedia } from "../../../models/CustomerMedia";
import { startTransaction } from "../../../core/sequelize";

const schema = Joi.object({
  customerId: Joi.string().max(45).required(),
  name      : Joi.string().max(45).required(),
  url       : Joi.string().max(100).required()
});

type IBody = {
  customerId : string;
  name : string;
  url : string;
}

export default Method.post("/customer_media", schema, async (req: Request, res: Response) => {
  const {
    customerId,
    name,
    url
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {

  const customerMedia = new CustomerMedia({
    customerId,
    name,
    url
  });
      
  await customerMedia.save();

  res.json( customerMedia );
} catch (err) {
  await rollback();

  throw err;
}
});
});