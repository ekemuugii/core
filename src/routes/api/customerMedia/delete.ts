import { Method ,NotfoundError} from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerMedia } from "../../../models/CustomerMedia";

export default Method.del(`/customer_media/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
  const id: string = req.params.id;


  const customerMedia = await CustomerMedia.findOne({ where: { id } });
  if (!customerMedia) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагчийн файл олдсонгүй.");


  const result = await CustomerMedia.destroy({ where: { id: customerMedia.id } });
 
  res.json({ success: true });
});