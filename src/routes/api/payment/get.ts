import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Payment } from "../../../models/Payment";

export default Method.get(`/payment/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
   
  const id: string = req.params.id;

  const payment = await Payment.findOne({ where: { id } });
   
  if (!payment) throw new NotfoundError(ERRORS.CUSTOMER_RELATED_PERSON_NOTFOUND, "ТӨЛБӨРИЙН МЭДЭЭЛЭЛ ОЛДСОНГҮЙ.");
  res.json(payment);
});