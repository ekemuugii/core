import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { ERRORS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { Payment } from "../../../models/Payment";

const Schema = Joi.object({
  amount               : Joi.number().required(),
  currency             : Joi.string().max(45).required(),
  description          : Joi.string().max(45).required(),
  paymentStatus        : Joi.string().max(45).required(),
  payerUserId          : Joi.string().max(45).required(),
  creditAccountId      : Joi.string().max(45).required(),
  creditAccountType    : Joi.string().max(45).required(),
  creditAccountName    : Joi.string().max(45).required(),
  creditAccountNumber  : Joi.string().max(45).required(),
  creditAccountCurrency: Joi.string().max(45).required(),
  debitAccountId       : Joi.string().max(45).required(),
  debitAccountType     : Joi.string().max(45).required(),
  debitAccountName     : Joi.string().max(45).required(),
  debitAccountNumber   : Joi.string().max(45).required(),
  debitAccountCurrency : Joi.string().max(45).required(),
  paidDate             : Joi.date().required()
});

  type IBody = {
    amount: number;
    currency: string;
    description: string;
    paymentStatus: string;
    payerUserId: string;
    creditAccountId: string;
    creditAccountType: string;
    creditAccountName: string;
    creditAccountNumber: string;
    creditAccountCurrency: string;
    debitAccountId: string;
    debitAccountType: string;
    debitAccountName: string;
    debitAccountNumber: string;
    debitAccountCurrency: string;
    paidDate : Date;
  }
export default Method.put(`/payment/:${Method.uuid("id")}`, Schema, async (req: Request, res: Response) => {
  const id: string = req.params.id;

  const {
    amount,
    currency,
    description,
    paymentStatus,
    payerUserId,
    creditAccountId,
    creditAccountType,
    creditAccountName,
    creditAccountNumber,
    creditAccountCurrency,
    debitAccountId,
    debitAccountType,
    debitAccountName,
    debitAccountNumber,
    debitAccountCurrency,
    paidDate
   
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
        
      const payment = await Payment.findOne({ where: { id }, transaction });
      if (!payment) throw new NotfoundError(ERRORS.CUSTOMER_RELATED_PERSON_NOTFOUND, "ТӨЛБӨРИЙН МЭДЭЭЛЭЛ ОЛДСОНГҮЙ.");
        
      payment.set({
        amount,
        currency,
        description,
        paymentStatus,
        payerUserId,
        creditAccountId,
        creditAccountType,
        creditAccountName,
        creditAccountNumber,
        creditAccountCurrency,
        debitAccountId,
        debitAccountType,
        debitAccountName,
        debitAccountNumber,
        debitAccountCurrency,
        paidDate
      });
      const result = await payment.save({ transaction });
        
      await commit();
  
      res.json({ success: true });
    } catch (err) {
      await rollback();
  
      throw err;
    }
  });
});