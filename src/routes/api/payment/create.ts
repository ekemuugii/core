import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { Payment } from "../../../models/Payment";
import { startTransaction } from "../../../core/sequelize";
import { PaymentDetail } from "../../../models/PaymentDetail";
import { LoanPaybackGraph } from "../../../models/LoanPaybackGraph";
import { Loan } from "../../../models/Loan";
const schema = Joi.object({
  loanId              : Joi.string().max(255).required(),
  amount               : Joi.number().required(),
  // currency             : Joi.string().max(45).required(),
  // description          : Joi.string().max(45).required(),
  // paymentStatus        : Joi.string().max(45).required(),
  payerUserId          : Joi.string().max(45).required(),
  // creditAccountId      : Joi.string().max(45).required(),
  // creditAccountType    : Joi.string().max(45).required(),
  // creditAccountName    : Joi.string().max(45).required(),
  // creditAccountNumber  : Joi.string().max(45).required(),
  // creditAccountCurrency: Joi.string().max(45).required(),
  // debitAccountId       : Joi.string().max(45).required(),
  // debitAccountType     : Joi.string().max(45).required(),
  // debitAccountName     : Joi.string().max(45).required(),
  // debitAccountNumber   : Joi.string().max(45).required(),
  // debitAccountCurrency : Joi.string().max(45).required(),
  paidDate             : Joi.date().required()
});
  
  type IBody = {
    
    amount: number;
    loanId: string;
    payerUserId: string;
    paidDate : Date;
  }

export default Method.post("/payment/create", schema, async (req: Request, res: Response) => {
  const {
    amount,
    loanId,
    payerUserId,
    paidDate
  }: IBody = req.body;

  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loanPaybackGraph = await LoanPaybackGraph.findAndCountAll({where: {loanId : loanId, payStatus : 0  },    order : [["created_at", "ASC"]],})
      if (!loanPaybackGraph) throw new NotfoundError(ERRORS.USER_NOTFOUND, " Олдсонгүй!!! ");
      const payment = new Payment({
        loanId,                                 // loanId nemsen
        amount :loanPaybackGraph.rows[0].totalPayAmount,
        payerUserId,
        paidDate
      });
      await payment.save({ transaction });


      // if(payment?.amount === loanPaybackGraph?.totalPayAmount){
      //   const paymentDetailNew = new PaymentDetail ({
      //     loanId                : loanId,
      //     loanPayGraphId        : loanPaybackGraph?.id,
      //     paymentId             : payment.id,
      //     amount                : amount,
      //     payerUserId           : payerUserId,
      //     paidDate              : paidDate,
      //     code                  : loanPaybackGraph.code

      // });
    
      // await paymentDetailNew.save({ transaction });
    // }
    // if(payment?.amount === loanPaybackGraph?.totalPayAmount){
      for(let n = 0 ; n < loanPaybackGraph.count ; n++) {
            const paymentDetailNew = new PaymentDetail ({
        loanId                : loanId,
        loanPayGraphId        : loanPaybackGraph.rows[n].id,
        paymentId             : payment.id,
        amount                : loanPaybackGraph.rows[n].mainLoanPayAmount,
        payerUserId           : payerUserId,
        paidDate              : paidDate,
        code                  : loanPaybackGraph.rows[n].code

      });

    await paymentDetailNew.save({ transaction });
   
  }
  const loanPaybackGraphSet = await LoanPaybackGraph.findOne({where: {loanId : loanId },    order : [["created_at", "ASC"]],})
  if (!loanPaybackGraphSet) throw new NotfoundError(ERRORS.USER_NOTFOUND, " Олдсонгүй!!! ");
  loanPaybackGraphSet.set({
    loanId                : loanId,
    payStatus             :1
  });
  const loan = await Loan.findOne({where: {id : loanId },    order : [["created_at", "ASC"]],})
  if (!loan) throw new NotfoundError(ERRORS.USER_NOTFOUND, " Олдсонгүй!!! ");
  loan.set({
    id                : loanId,
    loanStatusId       : "4"
  });
      await commit();
      res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
    }
  });
});