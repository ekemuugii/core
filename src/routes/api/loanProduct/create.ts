import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProduct } from "../../../models/LoanProduct";

const schema = Joi.object({
    name              : Joi.string().max(255).required(),
    isActive          : Joi.boolean().required(),
    startDate         : Joi.date().required(),
    endDate           : Joi.date().required(),
    currencyId        : Joi.string().max(255).required(),
    dayCalcId         : Joi.string().max(255).required(),
    loanCategoryId    : Joi.string().max(255).required(),
    isMobile          : Joi.boolean().required(),
    productId         : Joi.string().max(255).required(),
    isPayFirstOffBal  : Joi.boolean().required(),
    rateTypeId          : Joi.string().max(255).required(),
});

type IBody = {
  name            : string;
  isActive        : boolean;
  startDate       : Date;
  endDate         : Date;
  currencyId      : string;
  dayCalcId       : string;
  loanCategoryId  : string;
  isMobile        : boolean;
  productId       : string;
  isPayFirstOffBal: boolean;
  rateTypeId        : string;

} 

export default Method.post("/LoanProduct/create", schema, async (req: Request, res: Response) => {
  console.log(req.body);
  const {
    name,
    isActive,
    startDate,
    endDate,
    currencyId,
    dayCalcId,
    loanCategoryId,
    isMobile,
    productId,
    isPayFirstOffBal,
    rateTypeId
    
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      const loan = await LoanProduct.findOne({ where: { name : name }, transaction });
        if (loan) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Хэрэглэгч бүртгэгдсэн байна.");

      const loanProductNew = new LoanProduct({
        name            : name,
        isActive        : isActive,
        startDate       : startDate,
        endDate         : endDate,
        currencyId      : currencyId,
        dayCalcId       : dayCalcId,
        loanCategoryId  : loanCategoryId,
        isMobile        : isMobile,
        productId       : productId,
        isPayFirstOffBal: isPayFirstOffBal,
        rateTypeId        : rateTypeId

      });

      const result =  await loanProductNew.save({ transaction });
        
      await commit();
 
      res.json({ success: result });
    } catch (err) {
      await rollback();

 
      throw err;
    }
  });
});