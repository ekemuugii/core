import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProduct } from "../../../models/LoanProduct";

export default Method.get(`/LoanProduct/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const loan = await LoanProduct.findOne({ where: { id } });

  res.json(loan);
});