import { Method } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { Op, Sequelize } from "sequelize";
import { IFilter } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { LoanProduct } from "../../../models/LoanProduct";
import { DayCalc } from "../../../models/DayCalc";
import { LoanCategory } from "../../../models/LoanCategory";
import { RateType } from "../../../models/RateType";
import { Product } from "../../../models/Product";
import { Currency } from "../../../models/Currency";

const schema = Joi.object({
  filter: Joi.object({
    query : Joi.string().max(100).optional().allow(null, ""),
    query2: Joi.string().max(100).optional().allow(null, "")
  }).required(),
  offset: Joi.object({
    page : Joi.number().integer().required(),
    limit: Joi.number().integer().required()
  }).required()
});

type IQuery = {
  filter: {
    query?: string;
    query2?: string;
  };
  offset: {
    page: string;
    limit: string;
  }
}

export default Method.get("/LoanProduct/list", schema, async (req: Request, res: Response) => {
  const { filter, offset } = req.query as IQuery;

  const filters: IFilter = {};

  if (filter.query && filter.query !== "" && filter.query !== null) {
    const query = filter.query.toLowerCase();
    
    filters[Op.or] = [
      Sequelize.where(Sequelize.fn("lower", Sequelize.col("name")), "LIKE", `%${query}%`),
      
    ];
  }

  const { rows, count } = await LoanProduct.findAndCountAll({
    where : filters,
    include: [{
      model: DayCalc,
      as   : "dayCalc"
    },
    {
      model : LoanCategory,
      as    : "loanCategory"
    },
    {
      model : RateType,
      as    : "rateType"
    },
    {
      model : Product,
      as    : "product"
    },
    {
      model : Currency,
      as    : "currency"
    }],
    order : [["created_at", "DESC"]],
    offset: (parseInt(offset.page, 10) -1) * parseInt(offset.limit, 10),
    limit : parseInt(offset.limit, 10)
  });

  res.json({
    count,
    rows: rows.map((row) => ({
      ...row.toJSON()
    }))
  });
});
