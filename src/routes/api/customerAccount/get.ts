import { Method } from "@goodtechsoft/xs-micro-service";
// import Joi from "joi";
// import { ERRORS, USER_STATUS } from "../../../constants";
// import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAccount } from "../../../models/CustomerAccount";

export default Method.get(`/customer_account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {

  const id: string = req.params.id;

  const customer = await CustomerAccount.findOne({ where: {id } });

  res.json(customer);
});


