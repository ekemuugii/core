import { Method, NotfoundError } from "@goodtechsoft/xs-micro-service";
import { ERRORS } from "../../../constants";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAccount } from "../../../models/CustomerAccount";
    

export default Method.del(`/customer_account/:${Method.uuid("id")}`, null, async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const customerAccount = await CustomerAccount.findOne({ where: { id } });
    if (!customerAccount) throw new NotfoundError(ERRORS.USER_NOTFOUND, "Харилцагч олдсонгүй.");
    
    await CustomerAccount.destroy({ where: { id} });
    
    res.json({ success: true });
});
