import { Method, NotfoundError, ValidationError } from "@goodtechsoft/xs-micro-service";
import Joi from "joi";
import { UUID, UUIDV4 } from "sequelize";
import { ERRORS, USER_STATUS } from "../../../constants";
import { startTransaction } from "../../../core/sequelize";
import { Request, Response } from "../../../middlewares/sign";
import { CustomerAccount } from "../../../models/CustomerAccount";
import { Customer } from "../../../models/Customer";

const schema = Joi.object({
  customerId      : Joi.string().max(100).optional().allow(null, ""),
  bankId      : Joi.string().required(),
  accountNumber   : Joi.number().required(),

});

type IBody = {
  customerId     : String;
  bankId     : string ;
  accountNumber  : Number ;
}

export default Method.post("/customer_account/create", schema, async (req: Request, res: Response) => {
  console.log(req.body);
  const {
    customerId,
    bankId,
    accountNumber,
    
       
  }: IBody = req.body;
  
  return startTransaction(async (commit, rollback, transaction) => {
    try {
      // const customer = await Customer.findOne({where :{ id :customerId} , transaction }); 

      // if (!customer) throw new ValidationError(ERRORS.USER_ALREADY_REGISTERED, "Хэрэглэгч бүртгэгдсэн байна.");

      const customerNew = new CustomerAccount({
        customerId   : customerId,
        bankId       : bankId,
        accountNumber: accountNumber,
      
      });
    
      await customerNew.save({ transaction });
      
      await commit();

      res.json({ success: true });
    } catch (err) {
      await rollback();

      throw err;
      }
  });
});