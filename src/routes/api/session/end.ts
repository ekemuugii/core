import { Method } from "@goodtechsoft/xs-micro-service";
import { Request, Response } from "express";
import { signOut } from "../../../middlewares/sign";

export default Method.post("/session/end", null, async (req: Request, res: Response) => {
  signOut(res);
});