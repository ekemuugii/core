import { Routes } from "@goodtechsoft/xs-micro-service";
import { Router } from "express";
import { SESSION_SCOPE } from "../constants/SESSION_SCOPE";
import { auth } from "../middlewares/auth";
import { sign } from "../middlewares/sign";

export const routes = () => {
  const router = Router();

  router.use("/api", [ Routes(__dirname, [
    "/api/auth/login",
    "/api/mobile/auth/login",
    "/api/general/init",
    "/api/customer/create",
    "/api/mobile/customer/create",
    "/api/mobile/general/init",
    "/api/mobile/loan/getInfo",
  ])]);


  router.use("/api", [auth,Routes(__dirname, "/api")]);

  return router;
};