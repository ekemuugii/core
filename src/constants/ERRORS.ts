export const ERRORS = {
  NO_CREDENTIALS                            : "NO_CREDENTIALS",
  AUTHENTICATION_FAILED                     : "AUTHENTICATION_FAILED",
  PASSWORD_EXPIRED                          : "PASSWORD_EXPIRED",
  USER_SUSPENDED                            : "USER_SUSPENDED",
  USER_TERMINATED                           : "USER_TERMINATED",
  ALREADY_REGISTERED                        : "ALREADY_REGISTERED",
  USER_NOTFOUND                             : "USER_NOTFOUND",
  CLIENT_NOTFOUND                           : "CLIENT_NOTFOUND",
  CLIENT_ALREADY_REGISTERED                 : "CLIENT_ALREADY_REGISTERED",
  OTP_NOTFOUND                              : "OTP_NOTFOUND",
  INVALID_OTP_CODE                          : "INVALID_OTP_CODE",
  OTP_CODE_EXPIRE_IN                        : "OTP_CODE_EXPIRE_IN",
  INVALID_ACTION                            : "INVALID_ACTION",
  PASSWORD_REGEX_ERROR                      : "PASSWORD_REGEX_ERROR",
  BUSINESS_NOTFOUND                         : "BUSINESS_NOTFOUND",
  INVALID_OTP_CHARSET                       : "INVALID_OTP_CHARSET",
  INVALID_OLD_PASSWORD                      : "INVALID_OLD_PASSWORD",
  INVALID_EMAIL                             : "INVALID_EMAIL",
  INVALID_PIN                               : "INVALID_PIN",
  PARTNER_NOTFOUND                          : "PARTNER_NOTFOUND",
  USER_ALREADY_REGISTERED                   : "USER_ALREADY_REGISTERED",
  CUSTOMER_ALREADY_REGISTERED               : "ХЭРЭГЛЭГЧ_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_NOTFOUND                         : "ХЭРЭГЛЭГЧ_ОЛДСОНГҮЙ",
  CUSTOMER_ACCOUNT_ALREADY_REGISTERED       : "ХЭРЭГЛЭГЧИЙН_ДАНС_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_ACCOUNT_NOTFOUND                 : "ХЭРЭГЛЭГЧИЙН_ДАНС_ОЛДСОНГҮЙ",
  CUSTOMER_ADDRESS_ALREADY_REGISTED         : "ХЭРЭГЛЭГЧИЙН_ХАЯГ_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_ADDRESS_NOTFOUND                 : "ХЭРЭГЛЭГЧИЙН_ХАЯГ_ОЛДСОНГҮЙ",
  CUSTOMER_MEDIA_ALREADY_REGISTERED         : "ХЭРЭГЛЭГЧИЙН_ФАЙЛ_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_MEDIA_NOTFOUND                   : "ХЭРЭГЛЭГЧИЙН_ФАЙЛ_ОЛДСОНГҮЙ",
  CUSTOMER_RELATED_PERSON_ALREADY_REGISTERED: "ХЭРЭГЛЭГЧИЙН_ХАМААРАЛТАЙ_ХҮН_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_RELATED_PERSON_NOTFOUND          : "ХЭРЭГЛЭГЧИЙН_ХАМААРАЛТАЙ_ХҮН_ОЛДСОНГҮЙ",
  CUSTOMER_ORG_ALREADY_REGISTERED           : "ХЭРЭГЛЭГЧИЙН_БАЙГУУЛЛАГА_БҮРТГЭГДСЭН_БАЙНА",
  CUSTOMER_ORG_NOTFOUND                     : "ХЭРЭГЛЭГЧИЙН_БАЙГУУЛЛАГА_ОЛДСОНГҮЙ",
  DEPARTMRENT_UNIT_ALREADY_REGISTERED       : "ХЭЛТЭС_БҮРТГЭГДСЭН_БАЙНА",
  DEPARTMENT_UNIT_NOTFOUND                  : "ХЭЛТЭС_ОЛДСОНГҮЙ",
  EMPLOOYEE_UNIT_ALREADY_REGISTERED         : "АЖИЛТАН_БҮРТГЭГДСЭН_БАЙНА",
  EMPLOOYEE_UNIT_NOTFOUND                   : "АЖИЛТАН_ОЛДСОНГҮЙ",
  LOAN_CATEGORY_ALREADY_REGISTERED          : "ЗЭЭЛИЙН_АНГИЛАЛ_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_CATEGORY_NOTFOUND                    : "ЗЭЭЛИЙН_АНГИЛАЛ_ОЛДСОНГҮЙ",
  LOAN_ALREADY_REGISTERED                   : "ЗЭЭЛ_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_NOTFOUND                             : "ЗЭЭЛ_ОЛДСОНГҮЙ",
  LOAN_DETAIL_ALREADY_REGISTERED            : "ЗЭЭЛИЙН_ДЭЛГЭРЭНГҮЙ_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_DETAIL_NOTFOUND                      : "ЗЭЭЛИЙН_ДЭЛГЭРЭНГҮЙ_ОЛДСОНГҮЙ",
  LOAN_PAY_GRAPH_ALREADY_REGISTERED         : "ЗЭЭЛ_ТӨЛӨЛТИЙН_ГРАФ_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_PAY_GRAPH_NOTFOUND                   : "ЗЭЭЛ_ТӨЛӨЛТИЙН_ГРАФ_ОЛДСОНГҮЙ",
  LOAN_PRODUCT_ALREADY_REGISTERED           : "ЗЭЭЛИЙН_БҮТЭЭГДХҮҮН_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_PRODUCT_NOTFOUND                     : "ЗЭЭЛИЙН_БҮТЭЭГДХҮҮН_ОЛДСОНГҮЙ",
  LOAN_TYPE_ALREADY_REGISTERED              : "ЗЭЭЛИЙН_ТӨРӨЛ_БҮРТГЭГДСЭН_БАЙНА",
  LOAN_TYPE_NOTFOUND                        : "ЗЭЭЛИЙН_ТӨРӨЛ_ОЛДСОНГҮЙ",
  STAFF_ALREADY_REGISTERED                  : "АЖИЛТАН_БҮРТГЭГДСЭН_БАЙНА",
  STAFF_NOTFOUND                            : "АЖИЛТАН_ОЛДСОНГҮЙ"

};