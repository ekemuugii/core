import bcrypt from "bcryptjs";
import { DataTypes, Optional  } from "sequelize";
import { Column, IsUUID, PrimaryKey, Table, Model ,ForeignKey ,BelongsTo} from "sequelize-typescript";
import { Customer } from "./Customer";
import { Staff } from "./Staff";

export interface IUser {
  id?: string;
  isActive?: boolean;
  username: string;
  email?: string;
  isEmailVerified?: boolean;
  emailVerifiedDate?: Date; 
  phone?: string;
  isPhoneVerified?: boolean;
  phoneVerifiedDate?: Date;
  lastName?: string | null;
  firstName?: string | null;
  sessionScope?: string;
  password?: string;
  sessionId?: string;
  expiryHours?: number;
  userSuspended?: boolean;
  userTerminated?: boolean;
  passwordExpired?: boolean;
  passwordNeedChange?: boolean;
  customerId?: string;
  staffId?: string;
  expiryDate?: Date;
  userStatus: string;
  userStatusDate: Date;
  createdAt?: Date;
  updatedAt?: Date;
  validatePassword?: (password: string) => Promise<boolean>;
  createPassword?: (password: string) => Promise<string>;
}

type Attributes = Optional<IUser, "id">;

@Table({ tableName: "user", timestamps: true, paranoid: true })
export class User extends Model<IUser, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;

  @Column({ field: "session_id", type: DataTypes.STRING })
  declare sessionId: string;
  
  @Column({ field: "is_active", type: DataTypes.BOOLEAN, defaultValue: true })
  declare isActive: boolean;

  @Column({ field: "email", type: DataTypes.STRING })
  declare email: string;

  @Column({ field: "username", type: DataTypes.STRING })
  declare username: string;

  @Column({ field: "is_email_verified", type: DataTypes.BOOLEAN })
  declare isEmailVerified: boolean;

  @Column({ field: "email_verified_date", type: DataTypes.DATE })
  declare emailVerifiedDate: Date;

  @Column({ field: "phone", type: DataTypes.NUMBER })
  declare phone: number;

  @Column({ field: "is_phone_verified", type: DataTypes.BOOLEAN })
  declare isPhoneVerified: boolean;

  @Column({ field: "phone_verified_date", type: DataTypes.DATE })
  declare phoneVerifiedDate: Date;

  @Column({ field: "last_name", type: DataTypes.STRING })
  declare lastName: string;

  @Column({ field: "first_name", type: DataTypes.STRING })
  declare firstName: string;
  
  @Column({ field: "password", type: DataTypes.STRING })
  declare password: string;

  @Column({ field: "session_scope", type: DataTypes.STRING })
  declare sessionScope: string;

  @Column({ field: "expiry_hours", type: DataTypes.NUMBER })
  declare expiryHours: number;

  @Column({ field: "user_suspended", type: DataTypes.BOOLEAN, defaultValue: false })
  declare userSuspended: boolean;

  @Column({ field: "password_expired", type: DataTypes.BOOLEAN, defaultValue: false })
  declare passwordExpired: boolean;

  @Column({ field: "password_need_change", type: DataTypes.BOOLEAN, defaultValue: false })
  declare passwordNeedChange: boolean;

  @Column({ field: "user_terminated", type: DataTypes.BOOLEAN, defaultValue: false })
  declare userTerminated: boolean;

  @Column({ field: "expiry_date", type: DataTypes.DATE })
  declare expiryDate: Date;

  @Column({ field: "user_status", type: DataTypes.STRING })
  declare userStatus: string;

  @Column({ field: "user_status_date", type: DataTypes.DATE })
  declare userStatusDate: Date;

  

  @ForeignKey(() => Staff)
  @Column({ field: "staff_id" })
  declare staffId: string;
  @BelongsTo(() => Staff, "staffId")
  declare staff: Staff;


  @ForeignKey(() => Customer)
  @Column({ field: "customer_id" })
  declare customerId: string;
  @BelongsTo(() => Customer, "customerId")
  declare customer: Customer;


  async updatePassword (password: string){
    this.password = `some encrypts...${password}`;

    await this.save();
  }

  async validatePassword (password: string){
    if (!this.password) return false;

    const valid = await bcrypt.compare(password, this.password);
    return valid;
  }

  async createPassword (password: string){
    const SALT_ROUNDS = 10;

    const newPassword = await bcrypt.hash(password, SALT_ROUNDS);

    this.password = newPassword;

    return newPassword;
  }
}