import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IEthnicity {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name?: string;
  order?: string;
}

type Attributes = Optional<IEthnicity, "id">;

@Table({ tableName: "ethnicity", timestamps: true, paranoid: true })
export class Ethnicity extends Model<IEthnicity, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name", type: DataTypes.STRING })
  declare name: string;

  @Column({ field: "order", type: DataTypes.STRING })
  declare order: string;
}