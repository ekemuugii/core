import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
export interface ITransaction {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  type? : string;
  transactionStatus?: string;
  paymentId?: string;
  paymentMethod?: string;
  payerUserId?: string;
  creditAccountBank?: string;
  creditAccountId?: string;
  creditAccountNumber?: string;
  creditAccountCurrency?: string;
  creditAccountName?: string;
  debitAccountId?: string;
  debitAccountBank?: string;
  debitAccountNumber?: string;
  debitAccountName?: string;
  debitAccountCurrency?: string;
  description?: string;
  addInfo?: string; 
  amount?: string;
  hasTrxFee?: boolean;
  trxFee?: number;
  hasPaymentFee?: boolean;
  paymentFee?: number;
  hasBankTrxFee?: boolean;
  bankTrxFee?: number;
  totalAmount?: number;
  tranId?: string;
  tranDate?: string;
  tranStatus?: string;
}
 type Attributes = Optional<ITransaction, "id">;
 
@Table({ tableName: "transaction", timestamps: true, paranoid: true })
export class Transaction extends Model<ITransaction, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;
  @Column({ field: "updated_at" })
  declare updatedAt: Date;
  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "type", type: DataTypes.STRING })
  declare type: string;
  @Column({ field: "transaction_status", type: DataTypes.STRING })
  declare transactionStatus: string;

  @Column({ field: "payment_id", type: DataTypes.STRING })
  declare paymentId: string;
  @Column({ field: "payment_method", type: DataTypes.STRING })
  declare paymentMethod: string;
  @Column({ field: "payer_user_id", type: DataTypes.STRING })
  declare payerUserId: string;
  @Column({ field: "credit_account_bank", type: DataTypes.STRING })
  declare creditAccountBank: string;

  // @ForeignKey(() => Customer)
  @Column({ field: "credit_account_id", type: DataTypes.STRING })
  declare creditAccountId: string;
  // @BelongsTo(() => Customer, "customerId")
  // declare customer: Customer;
  @Column({ field: "credit_account_number", type: DataTypes.STRING })
  declare creditAccountNumber: string;
  @Column({ field: "credit_account_currency", type: DataTypes.STRING })
  declare creditAccountCurrency: string;
  @Column({ field: "credit_account_name", type: DataTypes.STRING })
  declare creditAccountName: string;

  @Column({ field: "debit_account_id", type: DataTypes.STRING })
  declare debitAccountId: string;

  @Column({ field: "debit_account_bank", type: DataTypes.STRING })
  declare debitAccountBank: string;
  @Column({ field: "debit_account_number", type: DataTypes.STRING })
  declare debitAccountNumber: string;
  @Column({ field: "debit_account_name", type: DataTypes.STRING })
  declare debitAccountName: string;
  @Column({ field: "debit_account_currency", type: DataTypes.STRING })
  declare debitAccountCurrency: string;
  @Column({ field: "description", type: DataTypes.STRING })
  declare description: string;
  @Column({ field: "add_info", type: DataTypes.STRING })
  declare addInfo: string;
  @Column({ field: "amount", type: DataTypes.STRING })
  declare amount: string;
  @Column({ field: "has_trx_fee", type: DataTypes.BOOLEAN })
  declare hasTrxFee: boolean;
  @Column({ field: "trx_fee", type: DataTypes.NUMBER })
  declare trxFee: number;
  @Column({ field: "has_payment_fee", type: DataTypes.BOOLEAN })
  declare hasPaymentFee: boolean;
  @Column({ field: "payment_fee", type: DataTypes.NUMBER })
  declare paymentFee: number;
  @Column({ field: "has_bank_trx_fee", type: DataTypes.BOOLEAN })
  declare hasBankTrxFee: boolean;
  @Column({ field: "bank_trx_fee", type: DataTypes.NUMBER })
  declare bankTrxFee: number;
  @Column({ field: "total_amount", type: DataTypes.NUMBER })
  declare totalAmount: number;

  @Column({ field: "tran_id", type: DataTypes.STRING })
  declare tranId: string;
  @Column({ field: "tran_date", type: DataTypes.STRING })
  declare tranDate: string;
  @Column({ field: "tran_status", type: DataTypes.STRING })
  declare tranStatus: string;
}