import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Loan } from "./Loan";

export interface ILoanPayGraph {
  id?            : string; 
  loanId?        : string;
  loanResidual?  : number;
  totalPayAmount?: number;
  rateCalcDay?   : number,
  mainLoanPayAmount?:number;
  rateAmount? : number;
  payDate?       : Date;
  createdAt?     : Date;
  updatedAt?     : Date;
  deletedAt?     : Date;
  code?          : string;            //nemsen
}

type Attributes = Optional<ILoanPayGraph, "id">;

@Table({ tableName: "loan_pay_graph", timestamps: true, paranoid: true })
export class LoanPayGraph extends Model<ILoanPayGraph, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "code", type: DataTypes.STRING })
  declare code: string;

  @ForeignKey(() => Loan)
  @Column({ field: "loan_id" })
  declare loanId: string;
  @BelongsTo(() => Loan, "loanId")
  declare loan: Loan;

  @Column({ field: "loan_residual", type: DataTypes.NUMBER })
  declare loanResidual: number;

  @Column({ field: "total_pay_amount", type: DataTypes.NUMBER })
  declare totalPayAmount: number;

  @Column({ field: "rate_calc_day", type: DataTypes.NUMBER })
  declare rateCalcDay: number;
  
  @Column({ field: "main_loan_pay_amount", type: DataTypes.NUMBER })
  declare mainLoanPayAmount: number;
  
  @Column({ field: "rate_amount", type: DataTypes.NUMBER })
  declare rateAmount: number;

  @Column({ field: "pay_date", type: DataTypes.DATE })
  declare payDate: Date;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;



}