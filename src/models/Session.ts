import { DataTypes, Optional } from "sequelize";
import { Column, IsUUID, PrimaryKey, Model, Table } from "sequelize-typescript";

export interface ISession {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  sessionID: string;
  deviceToken: string;
  deviceImei: string;
  isMobile: boolean;
  isDesktop: boolean;
  isBot: boolean;
  browser: string;
  version: string;
  os: string;
  platform: string;
  source: string;
}

type Attributes = Optional<ISession, "id">;

@Table({ tableName: "session", timestamps: true, paranoid: true })
export class Session extends Model<ISession, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;
  
  @Column({ field: "session_id", type: DataTypes.STRING })
  declare sessionID: string;

  @Column({ field: "device_token", type: DataTypes.STRING })
  declare deviceToken: string;

  @Column({ field: "device_imei", type: DataTypes.STRING })
  declare deviceImei: string;

  @Column({ field: "is_mobile", type: DataTypes.BOOLEAN })
  declare isMobile: boolean;

  @Column({ field: "is_desktop", type: DataTypes.BOOLEAN })
  declare isDesktop: boolean;

  @Column({ field: "is_bot", type: DataTypes.BOOLEAN })
  declare isBot: boolean;

  @Column({ field: "browser", type: DataTypes.BOOLEAN })
  declare browser: string;

  @Column({ field: "version", type: DataTypes.STRING })
  declare version: string;

  @Column({ field: "os", type: DataTypes.STRING })
  declare os: string;

  @Column({ field: "platform", type: DataTypes.STRING })
  declare platform: string;

  @Column({ field: "source", type: DataTypes.STRING })
  declare source: string;
}