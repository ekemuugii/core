import bcrypt from "bcryptjs";
import { DataTypes, Optional} from "sequelize";
import { Column, IsUUID, PrimaryKey, Table, Model ,ForeignKey ,BelongsTo} from "sequelize-typescript";
import { EducationType } from "./EducationType";
import { Gender } from "./Gender";
import { MarriageStatus } from "./MarriageStatus";
import { NationalityType } from "./NationalityType";
import { WorkStatus } from "./WorkStatus";
export interface ICustomer {
    id?: string;
    firstName?: string;
    lastName?: string ;
    familyName?: string ;
    birthDate?: Date ;
    registerNo?: string ;
    phone?: string;
    email?: string;
    signature?: string;
    educationTypeId?: string ;
    marriageStatusId?: string ;
    familyCount?: Number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    incomeAmountMonth?: Number;
    avatar?: string;
    nationalityTypeId?: string;
    genderId?: string ;
    birthPlace?: string ;
    birthPlaceNote?: string ;
    isActive?: boolean ;
    workStatusId? : string;
  }
   
type Attributes = Optional<ICustomer, "id">;

@Table({ tableName: "customer", timestamps: true, paranoid: true })
export class Customer extends Model<ICustomer, Attributes>{
   @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "family_name" , type: DataTypes.STRING })
  declare familyName: string;

  @Column({ field: "first_name", type: DataTypes.STRING })
  declare firstName: string;

  @Column({ field: "last_name", type: DataTypes.STRING })
  declare lastName: string;
  
  @Column({ field: "birth_date", type: DataTypes.DATE })
  declare birthDate: Date;

   @Column({ field: "register_no", type: DataTypes.STRING })
  declare registerNo: string; 

  @Column({ field: "phone", type: DataTypes.STRING })
  declare phone: string;

  @Column({ field: "email", type: DataTypes.NUMBER })
  declare email: string;  

  @Column({ field: "signature" , type: DataTypes.STRING})
  declare signature: string;  


  @ForeignKey(() => EducationType)
  @Column({ field: "education_type_id" })
  declare educationTypeId: string;
  @BelongsTo(() => EducationType, "educationTypeId")
  declare educationType: EducationType;


  @ForeignKey(() => MarriageStatus)
  @Column({ field: "marriage_status_id" })
  declare marriageStatusId: string;
  @BelongsTo(() => MarriageStatus, "marriageStatusId")
  declare marriageStatus: MarriageStatus;


  @Column({ field: "family_count", type: DataTypes.NUMBER })
  declare familyCount: Number;  

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;

  @Column({ field: "income_amount_month", type: DataTypes.NUMBER })
  declare incomeAmountMonth: Number;
  
  @Column({ field: "avatar", type: DataTypes.STRING })
  declare avatar: string;

  @ForeignKey(() => NationalityType)
  @Column({ field: "nationality_type_id" })
  declare nationalityTypeId: string;
  @BelongsTo(() => NationalityType, "nationalityTypeId")
  declare nationalityType: NationalityType;


  @ForeignKey(() => Gender)
  @Column({ field: "gender_id" })
  declare genderId: string;
  @BelongsTo(() => Gender, "genderId")
  declare gender: Gender;

  @ForeignKey(() => WorkStatus)
  @Column({ field: "work_status_id" })
  declare workStatusId: string;
  @BelongsTo(() => WorkStatus, "workStatusId")
  declare workStatus: WorkStatus;

  @Column({ field: "birth_place", type: DataTypes.STRING })
  declare birthPlace: string;  

  @Column({ field: "birth_place_note", type: DataTypes.STRING })
  declare birthPlaceNote: string;  

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;  


    }