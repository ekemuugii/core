import { date, string } from "joi";
import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Currency } from "./Currency";
import { HolidayCalc } from "./HolidayCalc";
import { Loan } from "./Loan";
import { LoanPayFreq } from "./LoanPayFreq";
import { LoanPayType } from "./LoanPayType";
import { LoanTime } from "./LoanTime";


export interface ILoanDetail {
  id?: string;
  loanId? : string;
  amount   : number;
  totalResidual? : number;
  currencyId? : string;
  currencyRate? : number;
  rate? : number;
  isActive?  : boolean;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  loanPayTypeId? : string;
  loanPayFreqId? : string;
  paymentDay1? : number;
  paymentDay2? : number;
  loanFreePayCount? : number;
  startDate? : Date;
  endDate? : Date;
  holidayCalcId? : string;
  

}

type Attributes = Optional<ILoanDetail, "id">;

@Table({ tableName: "loan_detail", timestamps: true, paranoid: true })
export class LoanDetail extends Model<ILoanDetail, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;
  
  @ForeignKey(() => Loan)
  @Column({ field: "loan_id" })
  declare loanId: string;
  @BelongsTo(() => Loan, "loanId")
  declare loan: Loan;

  @ForeignKey(() => HolidayCalc)
  @Column({ field: "holiday_calc_id" })
  declare holidayCalcId: string;
  @BelongsTo(() => HolidayCalc, "holidayCalcId")
  declare holidayCalc: HolidayCalc;

  @ForeignKey(() => Currency)
  @Column({ field: "currency_id" })
  declare currencyId: string;
  @BelongsTo(() => Currency, "currencyId")
  declare currency: Currency;
  
  @Column({ field: "currency_rate", type: DataTypes.STRING })
  declare currencyRate: number;


  @ForeignKey(() => LoanPayFreq)
  @Column({ field: "loan_pay_freq_id" })
  declare loanPayFreqId: string;
  @BelongsTo(() => LoanPayFreq, "loanPayFreqId")
  declare loanPayFreq: LoanPayFreq;



  @ForeignKey(() => LoanPayType)
  @Column({ field: "loan_pay_type_id" })
  declare loanPayTypeId: string;
  @BelongsTo(() => LoanPayType, "loanPayTypeId")
  declare loanPayType: LoanPayType;

  @Column({ field: "amount", type: DataTypes.NUMBER })
  declare amount: number;

  @Column({ field: "rate", type: DataTypes.NUMBER })
  declare rate: number;

  @Column({ field: "total_residual", type: DataTypes.NUMBER })
  declare totalResidual: number;

  @Column({ field: "payment_day1", type: DataTypes.NUMBER })
  declare paymentDay1: number;
  
  @Column({ field: "payment_day2", type: DataTypes.NUMBER })
  declare paymentDay2: number;
  
  @Column({ field: "start_date", type: DataTypes.DATE })
  declare startDate: Date;
  
  @Column({ field: "end_date", type: DataTypes.DATE })
  declare endDate: Date;

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;



}