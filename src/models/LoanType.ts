import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
export interface ILoanType {
  id?        : string; 
  name?      : string;
  isActive?  : boolean;
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
}

type Attributes = Optional<ILoanType, "id">;

@Table({ tableName: "loan_type", timestamps: true, paranoid: true })
export class LoanType extends Model<ILoanType, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "name", type: DataTypes.STRING })
  declare name: string;
  
  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;



}