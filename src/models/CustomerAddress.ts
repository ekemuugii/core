import { DataTypes, Optional } from "sequelize";
import { Column, IsUUID, PrimaryKey, Table, Model, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Khoroo } from "./Khoroo";
import { District } from "./District";
import { Province } from "./Province";
import { AddressType } from "./AddressType";

export interface ICustomerAddress {
  id? : string;
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  customerId? : string;
  addressTypeId? : string;
  provinceId? : string;
  districtId? : string;
  khorooId? : string;
  address? : string;
  isActive? : boolean;
  createdUserId? : string;
  updatedUserId? : string | null;
}

type Attributes = Optional<ICustomerAddress, "id">;

@Table({ tableName: "customer_address", timestamps: true, paranoid: true })
export class CustomerAddress extends Model<ICustomerAddress, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;
    
  // @ForeignKey(() => User)
  @Column({ field: "customer_id", type: DataTypes.STRING })
  declare customerId: string;
  // @BelongsTo(() => User, "customerId")
  // declare customer: User;

  // @Column({ field: "address_type", type: DataTypes.STRING })
  // declare addressType: string;

  @ForeignKey(() => AddressType)
  @Column({ field: "address_type_id" })
  declare addressTypeId: string;
  @BelongsTo(() => AddressType, "addressTypeId")
  declare addressType: AddressType;
  
  @ForeignKey(() => Province)
  @Column({ field: "province_id" })
  declare provinceId: string;
  @BelongsTo(() => Province, "provinceId")
  declare province: Province;

  @ForeignKey(() => District)
  @Column({ field: "district_id" })
  declare districtId: string;
  @BelongsTo(() => District, "districtId")
  declare district: District;

  @ForeignKey(() => Khoroo)
  @Column({ field: "khoroo_id" })
  declare khorooId: string;
  @BelongsTo(() => Khoroo, "khorooId")
  declare khoroo: Khoroo;

    
  @Column({ field: "address", type: DataTypes.STRING })
  declare address: string;

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;
    
  @Column({ field: "created_user_id", type: DataTypes.STRING })
  declare createdUserId: string;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;
    
  @Column({ field: "updated_user_id", type: DataTypes.STRING })
  declare updatedUserId: string;
    
  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;
            
}