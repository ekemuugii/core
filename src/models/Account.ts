import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Customer } from "./Customer";

export interface IAccount {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name?: string ;
  customerId?: string ;
  loanAmount? : number,
  scoreAmount? : number;
  accountNumber?:Number;
  balance? : number;
}

type Attributes = Optional<IAccount, "id">;

@Table({ tableName: "account", timestamps: true, paranoid: true })
export class Account extends Model<IAccount, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name" })
  declare name: string;

  @Column({ field: "account_number" })
  declare accountNumber: number;

  @Column({ field: "balance" })
  declare balance: number;

  @Column({ field: "score_amount" })
  declare scoreAmount: number;

  @Column({ field: "loan_amount" })
  declare loanAmount: number;

  @Column({ field: "is_active" })
  declare isActive: number;

  @ForeignKey(() => Customer)
  @Column({ field: "customer_id", type: DataTypes.STRING })
  declare customerId: string;
   @BelongsTo(() => Customer, "customerId")
   declare customer: Customer;

}