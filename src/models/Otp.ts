import randomString from "randomstring";
import bcrypt from "bcryptjs";
import moment from "moment";
import { ERRORS, OTP_CHARSET } from "../constants";
import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { ValidationError } from "@goodtechsoft/xs-micro-service";
import { User } from "./User";

export interface IOtp {
  id?: string;
  userId: string;
  otpMethod: string;
  otpMethodSent: Date;
  otpCharset: string;
  otpCode?: string;
  otpStatus: string;
  otpStatusDate: Date;
  validateCode?: (code: string) => Promise<boolean>;
  validateExpiryIn?: () => Promise<boolean>;
  generateCode?: () => Promise<string>;
}

type Attributes = Optional<IOtp, "id">;

@Table({ tableName: "otp", timestamps: true, paranoid: true })
export class Otp extends Model<IOtp, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: "user_id", type: DataTypes.STRING })
  declare userId: string;

  @BelongsTo(() => User, "userId")
  declare user: User;

  @Column({ field: "otp_method", type: DataTypes.STRING })
  declare otpMethod: string;

  @Column({ field: "otp_charset", type: DataTypes.STRING })
  declare otpCharset: string;

  @Column({ field: "otp_code", type: DataTypes.STRING })
  declare otpCode: string;

  @Column({ field: "otp_method_sent", type: DataTypes.DATE })
  declare otpMethodSent: Date;

  @Column({ field: "otp_status", type: DataTypes.STRING })
  declare otpStatus: string;

  @Column({ field: "otp_status_date", type: DataTypes.DATE })
  declare otpStatusDate: Date;

  async validateCode (code: string){
    if (!await bcrypt.compare(code, this.otpCode)) 
      return false;

    return true;
  }

  async validateExpiryIn (){
    // const duration = moment.duration(moment(this.otpMethodSent).add(3, "minute").diff(moment(new Date())));
    // if (duration.asSeconds() <= 0) 
    //   return false;

    return true;
  }

  async generateCode (){
    let rand;

    switch (this.otpCharset) {
    case OTP_CHARSET.ALPHABETIC: {
      rand = randomString.generate({ length: 50, charset: "alphabetic" });
      break;
    }
    case OTP_CHARSET.NUMERIC: {
      rand = randomString.generate({
        length : 4,
        charset: "numeric"
      }); // Math.round(1000 + Math.random() * 899999);
      break;
    }
    default:
      throw new ValidationError(ERRORS.INVALID_OTP_CHARSET);
    }

    const OTP_CODE = await bcrypt.hash(`${rand}`, 10);

    this.otpCode = OTP_CODE;

    return rand;
  }
}