import { DataTypes, Optional } from "sequelize";
import { Column, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface ICustomerMedia {
  id? : string;
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  isActive? : boolean;

  customerId? : string;
  name? : string;
  url? : string;
  updatedUserId?: string;
  createdUserId?: string;
}

type Attributes = Optional<ICustomerMedia, "id">;

@Table({ tableName: "customer_media", timestamps: true, paranoid: true })
export class CustomerMedia extends Model<ICustomerMedia, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;
    
  @Column({ field: "customer_id", type: DataTypes.STRING })
  declare customerId: string;

  @Column({ field: "name", type: DataTypes.STRING })
  declare name: string;

  @Column({ field: "url", type: DataTypes.STRING })
  declare url: string;
   
  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;
    
  @Column({ field: "created_user_id", type: DataTypes.STRING })
  declare createdUserId: string;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;
    
  @Column({ field: "updated_user_id", type: DataTypes.DATE })
  declare updatedUserId: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;
           
}