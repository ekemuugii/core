import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Customer } from "./Customer";
import { Bank } from "./Bank";
export interface ICustomerAccount {
    id?: string;
    customerId?: String;
    bankId?: String ;
    accountNumber?: Number ;
    isActive?: boolean | null;
    createdAt?: Date;
    createdUserId?: Number | null;
    updatedAt?: Date;
    updatedUserId?: Number;
    deletedAt?: Date;

}
 
type Attributes = Optional<ICustomerAccount, "id">;

@Table({ tableName: "customer_account", timestamps: true, paranoid: true })
export class CustomerAccount extends Model<ICustomerAccount, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @ForeignKey(() => Customer)
  @Column({ field: "customer_id", type: DataTypes.STRING })
  declare customerId: string;
   @BelongsTo(() => Customer, "customerId")
   declare customer: Customer;

  @Column({ field: "is_original", type: DataTypes.NUMBER })
  declare isOriginal: number;

  
  @ForeignKey(() => Bank)
  @Column({ field: "bank_id" })
  declare bankId: string;
  @BelongsTo(() => Bank, "bankId")
  declare bank: Bank;


  @Column({ field: "account_number", type: DataTypes.NUMBER })
  declare accountNumber: number;

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean; 

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "created_user_id", type: DataTypes.NUMBER })
  declare createdUserId: Number;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "updated_user_id", type: DataTypes.NUMBER })
  declare updatedUserId: Number;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;
 
}