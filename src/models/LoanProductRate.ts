import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { AccrFreq } from "./AccrFreq";
import { CalcType } from "./CalcType";
import { CapFreq } from "./CapFreq";
import { LoanProduct } from "./LoanProduct";
import { LoanRateType } from "./LoanRateType";
export interface ILoanProductRate {
  id? : string; 
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  loanProductId? : string;
  loanRateTypeId? : string;
  calcTypeId? : string;
  minRate? : number;
  maxRate? : number;
  accrFreqId? : string;
  capFreqId? : string;
}

type Attributes = Optional<ILoanProductRate, "id">;

@Table({ tableName: "loan_product_rate", timestamps: true, paranoid: true })
export class LoanProductRate extends Model<ILoanProductRate, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @ForeignKey(() => LoanProduct)
  @Column({ field: "loan_product_id", type: DataTypes.STRING })
  declare loanProductId: string;
  @BelongsTo(() => LoanProduct, "loanProductId")
  declare loanProduct: LoanProduct;

  @ForeignKey(() => LoanRateType)
  @Column({ field: "loan_rate_type_id", type: DataTypes.STRING })
  declare loanRateTypeId: string;
  @BelongsTo(() => LoanRateType, "loanRateTypeId")
  declare loanRateType: LoanRateType;

  @ForeignKey(() => CalcType)
  @Column({ field: "calc_type_id", type: DataTypes.STRING })
  declare calcTypeId: string;
  @BelongsTo(() => CalcType, "calcTypeId")
  declare calcType: CalcType;

  @Column({ field: "min_rate", type: DataTypes.NUMBER })
  declare minRate: number;

  @Column({ field: "max_rate", type: DataTypes.NUMBER })
  declare maxRate: number;

  @ForeignKey(() => AccrFreq)
  @Column({ field: "accr_freq_id", type: DataTypes.STRING })
  declare accrFreqId: string;
  @BelongsTo(() => AccrFreq, "accrFreqId")
  declare accrFreq: AccrFreq;

  @ForeignKey(() => CapFreq)
  @Column({ field: "cap_freq_id", type: DataTypes.STRING })
  declare capFreqId: string;
  @BelongsTo(() => CapFreq, "capFreqId")
  declare capFreq: CapFreq;
  
  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

}