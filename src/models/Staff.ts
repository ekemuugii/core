import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IStaff {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  registerNo? : string;
  familyName? : string;
  firstName? : string;
  lastName? : string;
  avatar? : string;
  roleId? : string;
  departmentUnitId? : string; 
  employeeUnitId?  : string;
  roleEnableDate?  : Date;
  roleDisableDate?  : Date;
  phone?  : string;
  email?  : string;
  userStatus?  : string;
 
}

type Attributes = Optional<IStaff, "id">;

@Table({ tableName: "staff", timestamps: true, paranoid: true })
export class Staff extends Model<IStaff, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;
  
  @Column({ field: "register_no", type: DataTypes.STRING })
  declare registerNo: string;

  @Column({ field: "family_name", type: DataTypes.STRING })
  declare familyName: string;
  
  @Column({ field: "first_name", type: DataTypes.STRING })
  declare firstName: string;

  @Column({ field: "last_name", type: DataTypes.NUMBER })
  declare lastName: number;
  
  @Column({ field: "avatar", type: DataTypes.STRING })
  declare avatar: string;

  @Column({ field: "role_id", type: DataTypes.STRING })
  declare roleId: string;

  @Column({ field: "department_unit_id", type: DataTypes.STRING })
  declare departmentUnitId: string;

  @Column({ field: "employee_unit_id", type: DataTypes.STRING })
  declare employeeUnitId: string;

  @Column({ field: "role_enable_date", type: DataTypes.DATE })
  declare roleEnableDate: Date;

  @Column({ field: "role_disable_date", type: DataTypes.DATE })
  declare roleDisableDate: Date;

  @Column({ field: "phone", type: DataTypes.STRING })
  declare phone: string;

  @Column({ field: "email", type: DataTypes.STRING })
  declare email: string;

  @Column({ field: "user_status", type: DataTypes.STRING })
  declare userStatus: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;
}