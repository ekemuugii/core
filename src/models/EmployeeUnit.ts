import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";


export interface IEmployeeUnit {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name?: string ;
  departmentUnitId? : string

}

type Attributes = Optional<IEmployeeUnit, "id">;

@Table({ tableName: "employee_unit", timestamps: true, paranoid: true })
export class EmployeeUnit extends Model<IEmployeeUnit, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name" })
  declare name: string;

  @Column({ field: "department_unit_id" })
  declare departmentUnitId: string;
}