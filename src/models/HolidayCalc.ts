import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IHolidayCalc {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name: string;
}

type Attributes = Optional<IHolidayCalc, "id">;

@Table({ tableName: "holiday_calc", timestamps: true, paranoid: true })
export class HolidayCalc extends Model<IHolidayCalc, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name" })
  declare name: string;
}