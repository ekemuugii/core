import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { User } from "./User";

export interface IAttempt {
  id?: string;
  userId: string;
  user?: User;
  sessionId: string;
  ipAddress: string| null;
  attemptStatus: string;
  attemptStatusDate: Date
}

type Attributes = Optional<IAttempt, "id">;

@Table({ tableName: "attempt", timestamps: true, paranoid: true })
export class Attempt extends Model<IAttempt, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: "user_id", type: DataTypes.STRING })
  declare userId: string;

  @BelongsTo(() => User, "userId")
  declare user: User;

  @Column({ field: "session_id", type: DataTypes.STRING })
  declare sessionId: string;

  @Column({ field: "ip_address", type: DataTypes.STRING })
  declare ipAddress: string;

  @Column({ field: "attempt_status", type: DataTypes.STRING })
  declare attemptStatus: string;

  @Column({ field: "attempt_status_date", type: DataTypes.DATE })
  declare attemptStatusDate: Date;
}