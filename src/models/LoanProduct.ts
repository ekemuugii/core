import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Currency}  from "../models/Currency";
import  { DayCalc } from "./DayCalc";
import { LoanCategory } from "./LoanCategory";
import { Product } from "./Product";
import { RateType } from "./RateType";
export interface ILoanProduct {
  id?        : string; 
  name?      : string;
  isActive?  : boolean;
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  startDate? : Date;
  endDate? : Date;
  currencyId? : string;
  dayCalcId? : string;
  loanCategoryId? : string;
  isMobile? : boolean;
  productId? : string;
  isPayFirstOffBal? : boolean;
  rateTypeId? : string;
}

type Attributes = Optional<ILoanProduct, "id">;

@Table({ tableName: "loan_product", timestamps: true, paranoid: true })
export class LoanProduct extends Model<ILoanProduct, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "name", type: DataTypes.STRING })
  declare name: string;
  
  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "created_by" })
  declare createdBy: string;

  @Column({ field: "updated_by" })
  declare updatedBy: string;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "start_date" })
  declare startDate: Date;
  @Column({ field: "end_date" })
  declare endDate: Date;

  @ForeignKey(() => Currency)
  @Column({ field: "currency_id" })
  declare currencyId: string;
  @BelongsTo(() => Currency, "currencyId")
  declare currency: Currency;

  @ForeignKey(() => DayCalc)
  @Column({ field: "day_calc_id" })
  declare dayCalcId: string;
  @BelongsTo(() => DayCalc, "dayCalcId")
  declare dayCalc: DayCalc;

  @ForeignKey(() => LoanCategory)
  @Column({ field: "loan_category_id" })
  declare loanCategoryId: string;
  @BelongsTo(() => LoanCategory, "loanCategoryId")
  declare loanCategory: LoanCategory;

  @Column({ field: "is_mobile" })
  declare isMobile: Boolean;

  @Column({ field: "is_pay_first_off_bal" })
  declare isPayFirstOffBal: Boolean;

  @ForeignKey(() => Product)
  @Column({ field: "product_id" })
  declare productId: string;
  @BelongsTo(() => Product, "productId")
  declare product: Product;

  @ForeignKey(() => RateType)
  @Column({ field: "rate_type_id" })
  declare rateTypeId: string;
  @BelongsTo(() => RateType, "rateTypeId")
  declare rateType: RateType;


}