import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IRole {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name? : string;
  roleStatus? : string;

}

type Attributes = Optional<IRole, "id">;

@Table({ tableName: "role", timestamps: true, paranoid: true })
export class Role extends Model<IRole, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;
  
  @Column({ field: "name", type: DataTypes.STRING })
  declare name: string;

  @Column({ field: "role_status", type: DataTypes.STRING })
  declare roleStatus: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;



}