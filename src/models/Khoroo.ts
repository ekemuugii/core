import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { District } from "./District";

export interface IKhoroo {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  districtId: string;
  name: string;
}

type Attributes = Optional<IKhoroo, "id">;

@Table({ tableName: "khoroo", timestamps: true, paranoid: true })
export class Khoroo extends Model<IKhoroo, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @ForeignKey(() => District)
  @Column({ field: "district_id" })
  declare districtId: string;
  @BelongsTo(() => District, "districtId")
  declare district: District;

  @Column({ field: "name" })
  declare name: string;
}