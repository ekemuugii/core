import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Customer } from "./Customer";

export interface IBlackList {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  customerId?: string;
  isActive?: boolean;
}

type Attributes = Optional<IBlackList, "id">;

@Table({ tableName: "black_list", timestamps: true, paranoid: true })
export class BlackList extends Model<IBlackList, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @ForeignKey(() => Customer)
  @Column({ field: "customer_id" })
  declare customerId: string;
  @BelongsTo(() => Customer, "customerId")
  declare customer: Customer;

  @Column({ field: "is_active" })
  declare isActive: boolean;
}