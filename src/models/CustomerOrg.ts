import bcrypt from "bcryptjs";
import { DataTypes, Optional} from "sequelize";
import { Column, IsUUID, PrimaryKey, Table, Model ,ForeignKey ,BelongsTo} from "sequelize-typescript";
import { OrgType } from "./OrgType";
import { Industry } from "./Industry";
import { EmployeeUnit } from "./EmployeeUnit";
export interface ICustomerOrg {
    id?: string;
    name?: string;
    registerNo?: string ;
    stateRegNo?: string ;
    stateRegDate?: Date ;
    orgTypeId?: string ;
    industryId?: string;
    employeeUnitId? : string;
    email?: string;
    phone?: number;
    directorFirstName?: string ;
    directorLastName?: string ;
    directorRegisterNo?: string;
    directorPhone?: number;
    contactEmpFirstName?: string ;
    contactEmpLastName?: string ;
    contactEmpRegisterNo?: string;
    contactEmpPhone?: number;
    deletedAt?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    incomeAmountYear?: number;
    employeeCount?: number;
    isActive?: boolean ;
  }
   
type Attributes = Optional<ICustomerOrg, "id">;

@Table({ tableName: "customer_org", timestamps: true, paranoid: true })
export class CustomerOrg extends Model<ICustomerOrg, Attributes>{
   @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "name" , type: DataTypes.STRING })
  declare name: string;

   @Column({ field: "register_no", type: DataTypes.STRING })
  declare registerNo: string; 


  @Column({ field: "state_reg_no" , type: DataTypes.STRING })
  declare stateRegNo: string;

  @Column({ field: "state_reg_date", type: DataTypes.DATE })
  declare stateRegDate: Date;



  @ForeignKey(() => Industry)
  @Column({ field: "industry_id" })
  declare industryId: string;
  @BelongsTo(() => Industry, "industryId")
  declare industry: Industry;

  @ForeignKey(() => OrgType)
  @Column({ field: "org_type_id" })
  declare orgTypeId: string;
  @BelongsTo(() => OrgType, "orgTypeId")
  declare orgType: OrgType;

  @ForeignKey(() => EmployeeUnit)
  @Column({ field: "employee_unit_id" })
  declare employeeUnitId: string;
  @BelongsTo(() => EmployeeUnit, "employeeUnitId")
  declare employeeUnit: EmployeeUnit;

  @Column({ field: "phone", type: DataTypes.NUMBER })
  declare phone: Number;  

  @Column({ field: "email", type: DataTypes.NUMBER })
  declare email: string;  

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;


  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;

  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;

  @Column({ field: "director_firstname", type: DataTypes.STRING })
  declare directorFirstName: string;

  @Column({ field: "director_lastname", type: DataTypes.STRING })
  declare directorLastName: string;

  @Column({ field: "director_register_no", type: DataTypes.STRING })
  declare directorRegisterNo: string;

  @Column({ field: "director_phone", type: DataTypes.NUMBER })
  declare directorPhone: Number;

  @Column({ field: "contact_emp_firstname", type: DataTypes.STRING })
  declare contactEmpFirstName: string;

  @Column({ field: "contact_emp_lastname", type: DataTypes.STRING })
  declare contactEmpLastName: string;

  @Column({ field: "contact_emp_register_no", type: DataTypes.STRING })
  declare contactEmpRegisterNo: string;

  @Column({ field: "contact_emp_phone", type: DataTypes.NUMBER })
  declare contactEmpPhone: Number;


  @Column({ field: "income_amount_year", type: DataTypes.NUMBER })
  declare incomeAmountYear: Number;
  
  @Column({ field: "employee_count", type: DataTypes.NUMBER })
  declare employeeCount: Number;

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;  


    }