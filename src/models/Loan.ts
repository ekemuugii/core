import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Customer } from "./Customer";
import { LoanProduct } from "./LoanProduct";
import { Currency}  from "./Currency"
import { LoanTime } from "./LoanTime";
import { DayCalc } from "./DayCalc";
import { LoanType } from "./LoanType";
import { Product } from "./Product";
import { User } from "./User";
import { LoanStatus } from "./LoanStatus";
export interface ILoan {
  id?: string;
  productId? : string;
  loanProductId? : string;
  loanTypeId? : string;
  loanCategoryId? : string;
  amount? : number;
  loanRate? : number;
  loanDate? : Date;
  customerId? : string; 
  currencyId?: string;
  isActive?  : boolean;
  createdAt?: Date;
  createdBy?: string;
  updatedAt?: Date;
  deletedAt?: Date;
  isToZms?: boolean;
  isJudge?:boolean;
  loanTimeId ?: string;
  dayCalcId? : string;
  loanStatusId? : string;
}

type Attributes = Optional<ILoan, "id">;

@Table({ tableName: "loan", timestamps: true, paranoid: true })
export class Loan extends Model<ILoan, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;
  
  
  @ForeignKey(() => LoanType)
  @Column({ field: "loan_type_id" })
  declare loanTypeId: string;
  @BelongsTo(() => LoanType, "loanTypeId")
  declare loanType: LoanType;

  
  @ForeignKey(() => LoanProduct)
  @Column({ field: "loan_product_id" })
  declare loanProductId: string;
  @BelongsTo(() => LoanProduct, "loanProductId")
  declare loanProduct: LoanProduct;

  @ForeignKey(() => User)
  @Column({ field: "created_by" })
  declare createdBy: string;
  @BelongsTo(() => User, "createdBy")
  declare user: User;


  @ForeignKey(() => Product)
  @Column({ field: "product_id" })
  declare productId: string;
  @BelongsTo(() => Product, "productId")
  declare product: Product;

  @Column({ field: "loan_category_id", type: DataTypes.STRING })
  declare loanCategoryId: string;

  @Column({ field: "amount", type: DataTypes.NUMBER })
  declare amount: number;

  @Column({ field: "loan_rate", type: DataTypes.NUMBER })
  declare loanRate: number;

  @Column({ field: "loan_date", type: DataTypes.DATE })
  declare loanDate: Date;
  
  @ForeignKey(() => Customer)
  @Column({ field: "customer_id" })
  declare customerId: string;
  @BelongsTo(() => Customer, "customerId")
  declare customer: Customer;


  @ForeignKey(() => Currency)
  @Column({ field: "currency_id" })
  declare currencyId: string;
  @BelongsTo(() => Currency, "currencyId")
  declare currency: Currency;

  @ForeignKey(() => DayCalc)
  @Column({ field: "day_calc_id" })
  declare dayCalcId: string;
  @BelongsTo(() => DayCalc, "dayCalcId")
  declare dayCalc: DayCalc;

  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at" })
  declare createdAt: Date;


  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "is_to_zms" })
  declare isToZms: boolean;

  @Column({ field: "is_judge" })
  declare isJudge: boolean;


  @ForeignKey(() => LoanStatus)
  @Column({ field: "loan_status_id" })
  declare loanStatusId: string;
  @BelongsTo(() => LoanStatus, "loanStatusId")
  declare loanStatus: LoanStatus;


  @ForeignKey(() => LoanTime)
  @Column({ field: "loan_time_id" })
  declare loanTimeId: string;
  @BelongsTo(() => LoanTime, "loanTimeId")
  declare loanTime: LoanTime;

}