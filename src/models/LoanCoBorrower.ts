import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { BorrowType } from "./BorrowType";
import { Customer } from "./Customer";
import { Loan } from "./Loan";
import { WhoType } from "./WhoType";

export interface ILoanCoBorrower {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  loanId? : string;
  whoTypeId? : string;
  borrowTypeId?: string;
  customerId?: string;
  isActive?: boolean;
}

type Attributes = Optional<ILoanCoBorrower, "id">;

@Table({ tableName: "loan_co_borrower", timestamps: true, paranoid: true })
export class LoanCoBorrower extends Model<ILoanCoBorrower, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @ForeignKey(() => Customer)
  @Column({ field: "customer_id" })
  declare customerId: string;
  @BelongsTo(() => Customer, "customerId")
  declare customer: Customer;

  @ForeignKey(() => WhoType)
  @Column({ field: "who_type_id" })
  declare whoTypeId: string;
  @BelongsTo(() => WhoType, "whoTypeId")
  declare whoType: WhoType;

  @ForeignKey(() => BorrowType)
  @Column({ field: "borrow_type_id" })
  declare borrowTypeId: string;
  @BelongsTo(() => BorrowType, "borrowTypeId")
  declare borrowType: BorrowType;

  @ForeignKey(() => Loan)
  @Column({ field: "loan_id" })
  declare loanId: string;
  @BelongsTo(() => Loan, "loanId")
  declare loan: Loan;


  @Column({ field: "is_active" })
  declare isActive: boolean;
}