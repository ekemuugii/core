import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Loan } from "./Loan";
import { LoanPayGraph } from "./LoanPayGraph";
import { Payment } from "./Payment";

export interface IPaymentDetail {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  loanId? : string;
  loanPayGraphId?: string;
  paymentId?: string;
  amount?: number;
  currency?: string;
  description? : string;
  paymentStatus? : string;
  payerUserId? : string;
  paidDate?: Date;
  code? : string;
}

type Attributes = Optional<IPaymentDetail, "id">;

@Table({ tableName: "payment_detail", timestamps: true, paranoid: true })
export class PaymentDetail extends Model<IPaymentDetail, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @ForeignKey(() => Loan)
  @Column({ field: "loan_id" })
  declare loanId: string;
  @BelongsTo(() => Loan, "loanId")
  declare loan: Loan;
    
  @Column({ field: "code", type: DataTypes.STRING })
  declare code: string;

  @ForeignKey(() => LoanPayGraph)
  @Column({ field: "loan_pay_graph_id" })
  declare loanPayGraphId: string;
  @BelongsTo(() => LoanPayGraph, "loanPayGraphId")
  declare loanPayGraph: LoanPayGraph;

  @ForeignKey(() => Payment)
  @Column({ field: "payment_id" })
  declare paymentId: string;
  @BelongsTo(() => Payment, "paymentId")
  declare payment: Payment;
  
  @Column({ field: "amount", type: DataTypes.NUMBER })
  declare amount: number;
  
  @Column({ field: "currency", type: DataTypes.STRING })
  declare currency: string;

  @Column({ field: "description", type: DataTypes.STRING })
  declare description: string;

  @Column({ field: "payment_status", type: DataTypes.STRING })
  declare paymentStatus: string;

  @Column({ field: "payer_user_id", type: DataTypes.STRING })
  declare payerUserId: string;
  
  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "paid_date" })
  declare paidDate: Date;

}