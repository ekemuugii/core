import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";


export interface IDepartmentUnit {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name?: string ;
  parentId? : string

}

type Attributes = Optional<IDepartmentUnit, "id">;

@Table({ tableName: "department_unit", timestamps: true, paranoid: true })
export class DepartmentUnit extends Model<IDepartmentUnit, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name" })
  declare name: string;

  @Column({ field: "parent_id" })
  declare parentId: string;
}