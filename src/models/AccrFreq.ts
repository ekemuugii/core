import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IAccrFreq {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  name: string;
}

type Attributes = Optional<IAccrFreq, "id">;

@Table({ tableName: "accr_freq", timestamps: true, paranoid: true })
export class AccrFreq extends Model<IAccrFreq, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "name" })
  declare name: string;
}