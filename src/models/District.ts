import { DataTypes, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Province } from "./Province";

export interface IDistrict {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  provinceId: string;
  name: string;
}

type Attributes = Optional<IDistrict, "id">;

@Table({ tableName: "district", timestamps: true, paranoid: true })
export class District extends Model<IDistrict, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @ForeignKey(() => Province)
  @Column({ field: "province_id" })
  declare provinceId: string;
  @BelongsTo(() => Province, "provinceId")
  declare province: Province;

  @Column({ field: "name" })
  declare name: string;
}