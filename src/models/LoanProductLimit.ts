import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

import { LoanProduct } from "./LoanProduct";

export interface ILoanProductLimit {
  id? : string; 
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  loanProductId? : string;
  minLimit? : number;
  maxLimit? : number;
}

type Attributes = Optional<ILoanProductLimit, "id">;

@Table({ tableName: "loan_product_limit", timestamps: true, paranoid: true })
export class LoanProductLimit extends Model<ILoanProductLimit, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;


  @ForeignKey(() => LoanProduct)
  @Column({ field: "loan_product_id", type: DataTypes.STRING })
  declare loanProductId: string;
  @BelongsTo(() => LoanProduct, "loanProductId")
  declare loanProduct: LoanProduct;

  @Column({ field: "min_limit", type: DataTypes.NUMBER })
  declare minLimit: number;

  @Column({ field: "max_limit", type: DataTypes.NUMBER })
  declare maxLimit: number;
  
  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

}