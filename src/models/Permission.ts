import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface IPermission {
  id?        : string;
  createdAt? : Date;
  updatedAt? : Date;
  deletedAt? : Date;
  roleId?    : string;
  refCode?   : string;
  isRead?    : boolean;
  isEdit?    : boolean;
  isDelete?  : boolean;
}

type Attributes = Optional<IPermission, "id">;

@Table({ tableName: "permission", timestamps: true, paranoid: true })
export class Permission extends Model<IPermission, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @Column({ field: "role_id", type: DataTypes.STRING })
  declare roleId: string;

  @Column({ field: "ref_code", type: DataTypes.STRING })
  declare refCode: string;

  @Column({ field: "is_read", type: DataTypes.BOOLEAN })
  declare isRead: boolean;

  @Column({ field: "is_edit", type: DataTypes.BOOLEAN })
  declare isEdit: boolean;

  @Column({ field: "is_delete", type: DataTypes.BOOLEAN })
  declare isDelete: boolean;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;



}