// import bcrypt from "bcryptjs";
import { DataTypes, Optional } from "sequelize";
import { Column, IsUUID, PrimaryKey, Table, Model, ForeignKey, BelongsTo } from "sequelize-typescript";
import { WhoType } from "./WhoType";

export interface ICustomerRelatedPerson {
  id? : string;
  customerId? : string;
  whoTypeId? : string;
  firstName? : string;
  lastName? : string;
  phone? : string;
  isActive? : boolean;
  createdAt? : Date;
  createdUserId?: string;
  updatedAt? : Date;
  updatedUserId?: string;
  deletedAt? : Date;
}

type Attributes = Optional<ICustomerRelatedPerson, "id">;

@Table({ tableName: "customer_related_person", timestamps: true, paranoid: true })
export class CustomerRelatedPerson extends Model<ICustomerRelatedPerson, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4, type: DataTypes.UUIDV4 })
  declare id: string;
    
  @Column({ field: "customer_id", type: DataTypes.STRING })
  declare customerId: string;

  @ForeignKey(() => WhoType)
  @Column({ field: "who_type_id" })
  declare whoTypeId: string;
  @BelongsTo(() => WhoType, "whoTypeId")
  declare whoType: WhoType;

  @Column({ field: "firstname", type: DataTypes.STRING })
  declare firstName: string;

  @Column({ field: "lastname", type: DataTypes.STRING })
  declare lastName: string;

  @Column({ field: "phone", type: DataTypes.NUMBER })
  declare phone: number;
    
  @Column({ field: "is_active", type: DataTypes.BOOLEAN })
  declare isActive: boolean;

  @Column({ field: "created_at", type: DataTypes.DATE })
  declare createdAt: Date;
    
  @Column({ field: "created_user_id", type: DataTypes.STRING })
  declare createdUserId: string;

  @Column({ field: "updated_at", type: DataTypes.DATE })
  declare updatedAt: Date;
    
  @Column({ field: "updated_user_id", type: DataTypes.STRING })
  declare updatedUserId: string;
    
  @Column({ field: "deleted_at", type: DataTypes.DATE })
  declare deletedAt: Date;
            
}