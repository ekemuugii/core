import { DataTypes, IntegerDataType, Optional } from "sequelize";
import { BelongsTo, Column, ForeignKey, HasMany, IsUUID, Model, PrimaryKey, Table } from "sequelize-typescript";
import { Loan } from "./Loan";
export interface IPayment {
  id?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  amount? : number;
  currency?: string;
  description? : string;
  paymentStatus? : string;
  payerUserId? : string;
  creditAccountId? : string;
  creditAccountType? : string; 
  creditAccountName?: string;
  creditAccountNumber? : string;
  creditAccountCurrency?: string;
  debitAccountId ?:string;
  debitAccountType?: string;
  debitAccountName?: string;
  debitAccountNumber? : string;
  debitAccountCurrency? : string;
  paidDate?: Date;
  loanId? : string;   //loanId nemsen
}

type Attributes = Optional<IPayment, "id">;

@Table({ tableName: "payment", timestamps: true, paranoid: true })
export class Payment extends Model<IPayment, Attributes>{
  @IsUUID(4)
  @PrimaryKey
  @Column({ field: "id", defaultValue: DataTypes.UUIDV4 })
  declare id: string;

  @ForeignKey(() => Loan)
  @Column({ field: "loan_id" })                     // NEMSEN LOANID
  declare loanId: string;
  @BelongsTo(() => Loan, "loanId")
  declare loan: Loan;

  @Column({ field: "amount", type: DataTypes.NUMBER })
  declare amount: number;
  
  @Column({ field: "currency", type: DataTypes.STRING })
  declare currency: string;

  @Column({ field: "description", type: DataTypes.STRING })
  declare description: string;

  @Column({ field: "payment_status", type: DataTypes.STRING })
  declare paymentStatus: string;

  @Column({ field: "payer_user_id", type: DataTypes.STRING })
  declare payerUserId: string;
  
  @Column({ field: "credit_account_id", type: DataTypes.STRING })
  declare creditAccountId: string;

  @Column({ field: "credit_account_type", type: DataTypes.STRING })
  declare creditAccountType: string;

  @Column({ field: "credit_account_name", type: DataTypes.STRING })
  declare creditAccountName: string;

  @Column({ field: "credit_account_number", type: DataTypes.STRING })
  declare creditAccountNumber: string;

  @Column({ field: "credit_account_currency", type: DataTypes.STRING })
  declare creditAccountCurrency: string;

  @Column({ field: "debit_account_id", type: DataTypes.STRING })
  declare debitAccountId: string;

  @Column({ field: "debit_account_type", type: DataTypes.STRING })
  declare debitAccountType: string;

  @Column({ field: "debit_account_number", type: DataTypes.STRING })
  declare debitAccountNumber: string;

  @Column({ field: "debit_account_currency", type: DataTypes.STRING })
  declare debitAccountCurrency: string;

  @Column({ field: "created_at" })
  declare createdAt: Date;

  @Column({ field: "updated_at" })
  declare updatedAt: Date;

  @Column({ field: "deleted_at" })
  declare deletedAt: Date;

  @Column({ field: "paid_date" })
  declare paidDate: Date;

}