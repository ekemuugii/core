import express, { NextFunction } from "express";
import jwt from "jsonwebtoken";
import { config } from "../config";

export const getDaysInMonth = (year: number , month:number) => {
  return new Date(year, month, 0).getDate();
};
